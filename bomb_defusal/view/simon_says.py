from pyglet import clock

from bomb_defusal.view.components import Button
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.drawing import set_material_color
from bomb_defusal.view.utils.helper import colors


class SimonSays(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`SimonSays` class.

        :param bomb_defusal.model.SimonSays model: Model of the simon says module
        """
        super().__init__(model)

        self._buttons = {
            'Blue': Button((0, 0, 0), static='resources/components/simons_button.obj'),
            'Red': Button((0, 0, 0), static='resources/components/simons_button.obj', rotate_y=90),
            'Green': Button((0, 0, 0), static='resources/components/simons_button.obj', rotate_y=180),
            'Yellow': Button((0, 0, 0), static='resources/components/simons_button.obj', rotate_y=270),
        }

        for color, button in self._buttons.items():
            set_material_color(button._model, colors[color])

        self._flash_index = 0
        clock.schedule_interval(self._flash, .5)

    def hit_test(self, position, button, modifiers):
        """Tests if any of the buttons of the module is hit by the mouse"""
        if button != 1:
            return None, None, None

        for color, button in self._buttons.items():
            distance = button.hit_test(*position)
            if distance is not None:
                return self, color, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        """Performs the hit action on the selected button"""
        self._buttons[tag].press()
        self._model.press(tag)

    def hit_release(self, tag, button, modifiers):
        """Releases the current pressed button"""
        self._buttons[tag].release()

    def on_draw(self):
        """Draws the 3D view of the module"""
        [button.draw() for button in self._buttons.values()]

    # noinspection PyUnusedLocal
    def _flash(self, dt):
        """Flashes the buttons and armed LED of the module"""
        flash_order = self._model.flash_order
        if self._model.disarmed:
            self._set_active(None)
            clock.unschedule(self._flash)
            return

        self._flash_index += 1
        if self._flash_index < 5:
            return

        if self._flash_index - 5 >= len(flash_order):
            self._flash_index = 0
            self._set_active(None)
            return

        self._set_active(flash_order[self._flash_index - 5])

    def _set_active(self, color):
        """Sets the button with the given color as the active button"""
        for c, button in self._buttons.items():
            if c == color:
                set_material_color(button._model, colors[c], emissive=colors[c])
            else:
                set_material_color(button._model, colors[c])
