from datetime import datetime, timedelta

from pyglet.gl import *
from pyglet.image import load as load_image
from pywavefront.material import Material
from pywavefront.mesh import Mesh
from pywavefront.texture import Texture

from bomb_defusal.view.utils.drawing import translate, set_material_color, create_font_texture, scale, rotate_y
from bomb_defusal.view.utils.helper import load_model, get_hit_distance, matrix4x4, get_bounds, create_box, draw_box, \
    vector


class TextTexture(Texture):
    # noinspection PyMissingConstructor
    def __init__(self, texture):
        self.image = texture


class ImageTexture(object):
    def __init__(self, position, image=None, **kwargs):
        self._image = None
        self._texture = None
        self._vertices = [(-.5, 0, .5), (.5, 0, .5), (.5, 0, -.5), (-.5, 0, -.5)]
        self._uv = [(0, 0), (0, 1), (1, 1), (1, 0)]

        self.image = image

        scale = kwargs.get('scale', (1, 1, 1))
        self._vertices = [(p[0] * scale[0], p[1] * scale[1], p[2] * scale[2]) for p in self._vertices]
        self._vertices = [(p[0] + position[0], p[1] + position[1], p[2] + position[2]) for p in self._vertices]

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._image = value
        self._texture = load_image('resources/keypad.png').get_texture() if value else None

    @property
    def uv(self):
        return self._uv

    @uv.setter
    def uv(self, value):
        self._uv = value

    def draw(self):
        if not self._texture:
            return

        glEnable(self._texture.target)
        glBindTexture(self._texture.target, self._texture.id)

        glBegin(GL_QUADS)
        for i in range(4):
            glTexCoord2f(*self._uv[i])
            glVertex3f(*self._vertices[i])
        glEnd()
        glDisable(self._texture.target)


class Static(object):
    def __init__(self, position, static, **kwargs):
        self._name = kwargs.get('name', None)
        self._hit_on_box = kwargs.get('hit_on_box', False)
        self._model_matrix = matrix4x4()
        self._model = load_model(static)
        self._position = position
        self._drawables = []

        if 'scale' in kwargs:
            scale(self._model, *kwargs.get('scale'))
        if 'rotate_y' in kwargs:
            rotate_y(self._model, kwargs.get('rotate_y'))
        translate(self._model, *position)

        bounds = get_bounds(self._model)
        self.bounding_box = create_box(*bounds)

    @property
    def name(self):
        return self._name

    def set_color(self, ambient, diffuse=None, specular=None, emissive=None, material=None):
        set_material_color(self._model if not material else self._model.materials[material],
                           ambient, diffuse, specular, emissive)

    def add_drawable(self, drawable):
        if drawable and drawable not in self._drawables:
            self._drawables.append(drawable)

    def before_draw(self):
        pass

    def draw(self):
        glPushMatrix()

        self.before_draw()
        self._model.draw()
        [drawable.draw() for drawable in self._drawables]

        glGetDoublev(GL_MODELVIEW_MATRIX, self._model_matrix)
        glPopMatrix()

    def hit_test(self, x, y):
        distance = get_hit_distance(self.bounding_box, (x, y), self._model_matrix)
        if distance is None:
            return None

        if self._hit_on_box:
            return distance

        for mesh in self._model.meshes.values():
            distance = get_hit_distance(mesh, (x, y), self._model_matrix)
            if distance is not None:
                return distance
        return None

    def remove_drawable(self, drawable):
        self._drawables.remove(drawable)


class Text(object):
    def __init__(self, position, **kwargs):
        self._position = position
        self._config = kwargs
        self._text = ''
        self._update_texture()

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
        self._update_texture()

    def draw(self):
        if self._text_mesh:
            self._text_mesh.draw()

    def _update_texture(self):
        if not self._text:
            self._text_mesh = None
            return

        material = Material('Text')
        material.vertices = []
        self._text_mesh = Mesh()
        self._text_mesh.add_material(material)

        height = self._config.get('text_height', .08)
        font_file = self._config.get('text_font', 'resources/fonts/sans.ttf')
        foreground = self._config.get('foreground', (0, 0, 0))
        background = self._config.get('background', (0, 0, 0, 0))
        offset = self._config.get('text_offset', .01)

        # TODO: find a way to scale the text to the full size of the texture
        texture = create_font_texture(self._text, background, foreground, font=font_file)
        material.texture = TextTexture(texture)
        set_material_color(material, [foreground[0], foreground[1], foreground[2], 1])

        width = height * texture.width / texture.height
        for vertex in [(0, 0), (0, 1), (1, 1), (0, 0), (1, 1), (1, 0)]:
            material.vertices.extend(vertex[:])
            material.vertices.extend([0, 1, 0])
            material.vertices.extend([(-width / 2) + vertex[0] * width, offset, (-height / 1.5) + vertex[1] * height])

        if 'rotate_y' in self._config:
            rotate_y(self._text_mesh, self._config['rotate_y'])
        translate(self._text_mesh, *self._position)


class Led(Static):
    def __init__(self, position, **kwargs):
        super().__init__(position, static='resources/components/led.obj', **kwargs)
        self._color = kwargs.get('color', (0, 0, 0, 0))
        self._active = kwargs.get('active', False)
        self._active_color = kwargs.get('active_color', None)
        self._update()

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, value):
        self._active = value
        self._update()

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value
        self._update()

    def _update(self):
        color = self._color
        if self._active and self._active_color:
            color = self._active_color
        set_material_color(self._model.materials['LED'],
                           color, color, color,
                           color if self._active else (.2, .2, .2, 1))


class Lcd(Static):
    def __init__(self, position, **kwargs):
        super().__init__(position, static=kwargs.pop('static', 'resources/components/lcd.obj'), **kwargs)
        self._text = Text(position, **kwargs)
        self.add_drawable(self._text)

    @property
    def text(self):
        return self._text.text

    @text.setter
    def text(self, value):
        self._text.text = value


class Button(Static):
    minimum_hold_duration = timedelta(seconds=.25)

    def __init__(self, position, **kwargs):
        super().__init__(position, static=kwargs.pop('static', 'resources/components/button1.obj'), **kwargs)
        self._config = kwargs
        self._label = None
        self._pressed = False
        self._min_release_time = None
        if 'label' in kwargs:
            self.text = kwargs.pop('label')

    @property
    def is_pressed(self):
        return self._pressed

    @property
    def text(self):
        if isinstance(self._label, Text):
            return self._label.text
        return None

    @text.setter
    def text(self, value):
        if not isinstance(self._label, Text):
            if self._label is not None:
                self.remove_drawable(self._label)
            self._label = Text(self._position, **self._config)
            self.add_drawable(self._label)
        elif self._label.text == value:
            return

        self._label.text = value

    @property
    def texture(self):
        return self._label

    @texture.setter
    def texture(self, value):
        if self._label == value:
            return

        if self._label is not None:
            self.remove_drawable(self._label)
        self.add_drawable(value)

        self._label = value

    def before_draw(self):
        if self._pressed or (self._min_release_time and datetime.utcnow() < self._min_release_time):
            glTranslatef(0, -.05, 0)

    def press(self):
        self._pressed = True
        self._min_release_time = datetime.utcnow() + Button.minimum_hold_duration

    def release(self):
        self._pressed = False


class HitBox(object):
    def __init__(self, position, vertices, color=None, emission=None):
        self._vertices = vertices
        self._color = color
        self._emission = emission
        self._model_matrix = matrix4x4()
        self._floats = vector(*vertices)
        self._faces = len(vertices) // 8

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value

    def hit_test(self, x, y):
        return get_hit_distance(self._vertices, (x, y), self._model_matrix)

    def draw(self):
        if self._color:
            specular = vector(*self._color)
            diffuse = vector(self._color[0] * .6, self._color[1] * .6, self._color[2] * .6, self._color[3])
            ambient = vector(self._color[0] * .4, self._color[1] * .4, self._color[2] * .4, self._color[3])
            emission = vector(*(self._emission or [0, 0, 0, 0]))

            glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse)
            glMaterialfv(GL_FRONT, GL_AMBIENT, ambient)
            glMaterialfv(GL_FRONT, GL_SPECULAR, specular)
            glMaterialfv(GL_FRONT, GL_EMISSION, emission)

            glInterleavedArrays(GL_T2F_N3F_V3F, 0, self._floats)
            glDrawArrays(GL_TRIANGLES, 0, self._faces)

        glGetDoublev(GL_MODELVIEW_MATRIX, self._model_matrix)
