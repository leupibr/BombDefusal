from pyglet import clock
from pyglet.gl import *

from bomb_defusal.view.components import Led, Button, Static, HitBox
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.drawing import set_material_color
from bomb_defusal.view.utils.helper import colors, get_bounds, create_box, get_hit_distance, load_model, draw_box, \
    create_box_from_vertices


class Wire(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Wire` class.

        :param bomb_defusal.model.Wire model: Model of the wire module
        """
        super().__init__(model, static='resources/wire.obj')

        self._wires = {
            'Wire{}'.format(index + 1): {
                'mesh': self._static.meshes['Wire{}'.format(index + 1)],
                'box': None,
                'cut': False,
                'color': colors[color],
                'index': index + 1,
            }
            for index, color in enumerate(self._model.wires)
        }

        for name, meta in self._wires.items():
            mesh = self._static.materials[name]
            meta['box'] = create_box(*get_bounds(mesh))
            set_material_color(mesh, meta['color'])

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        hit_distance = None
        hit_name = None

        for name, meta in self._wires.items():
            if meta['cut']:
                continue

            distance = get_hit_distance(meta['box'], position, self._model_matrix)
            if distance is not None:
                if not hit_name or distance < hit_distance:
                    hit_distance = distance
                    hit_name = name

        return self if hit_name else None, hit_name, hit_distance

    def hit(self, tag, button, modifiers):
        """Performs the hit action on the selected button"""
        if tag not in self._wires:
            return

        meta = self._wires[tag]
        meta['cut'] = True
        self._model.cut(meta['index'])

    def hit_release(self, tag, button, modifiers):
        self._static.mesh_list.remove(self._static.meshes[tag])


class WireSequence(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`WireSequence` class.

        :param bomb_defusal.model.WireSequence model: Model of the wire sequence module
        """
        super().__init__(model, static='resources/wire_sequence.obj')
        set_material_color(self._static.materials['Font'], (1, 1, 0, 1), emissive=(1, 1, 0, 1))

        self._door = Static((0, .01, 0), static='resources/components/wire_door.obj')
        self._wires = load_model('resources/components/wires.obj')
        self._leds = [Led((.5, 0, z)) for z in [.55, .4, .25, .1]]
        self._buttons = dict(
            U=Button((-.125, 0, -.5), static='resources/components/morse_button.obj', rotate_y=270, scale=(1.5, 1, 1.5)),
            D=Button((-.125, 0, .5), static='resources/components/morse_button.obj', rotate_y=90, scale=(1.5, 1, 1.5)),
        )

        self._wire_boxes = dict()
        for wire in ['1A', '1B', '1C', '2A', '2B', '2C', '3A', '3B', '3C']:
            self._wire_boxes['Wire{}_wire{}'.format(wire, wire.lower())] = self._create_wire_hit_box(wire[0], wire[1])

        self._alert = 5
        self._door_scale = .05
        self._door_closed_action = None
        self._update_stage()

        self._model.on_wrong_try += self._on_wrong_try
        clock.schedule_interval(self._tick, .01)

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        hit_distance = None
        hit_index = None

        for direction, button in self._buttons.items():
            distance = button.hit_test(*position)
            if distance:
                return self, direction, distance

        for index, config in enumerate(self._model.wires):
            if not config or config[2]:
                continue

            wire_name = 'Wire{0}{1}_wire{0}{2}'.format(index + 1, config[1], config[1].lower())
            distance = self._wire_boxes[wire_name].hit_test(*position)
            if distance is not None:
                if not hit_index or distance < hit_distance:
                    hit_distance = distance
                    hit_index = index

        return self if hit_index is not None else None, hit_index, hit_distance

    def hit(self, tag, button, modifiers):
        if tag in self._buttons:
            self._buttons[tag].press()
            self._animate_change(self._model.previous if tag == 'U' else self._model.next)

        if tag in range(3):
            self._model.cut(tag)
            self._update_stage()

    def hit_release(self, tag, button, modifiers):
        if tag in self._buttons:
            self._buttons[tag].release()

    def on_draw(self):
        for index, config in enumerate(self._model.wires):
            if not config:
                continue

            wire_name = 'Wire{0}{1}{3}_wire{0}{2}{3}'.format(
                index + 1,
                config[1],
                config[1].lower(),
                'cut' if config[2] else '')

            self._wires.meshes[wire_name].draw()

            if not config[2]:
                self._wire_boxes[wire_name].draw()

        [led.draw() for led in self._leds]
        [button.draw() for button in self._buttons.values()]

        for transform in ((-.5, 0), (.2, 180)):
            glPushMatrix()
            glTranslatef(transform[0], 0, 0)
            glRotatef(transform[1], 0, 1, 0)
            glScalef(min(1, self._door_scale), 1, 1)
            self._door.draw()
            glPopMatrix()

    def _create_wire_hit_box(self, start, end):
        side = .06
        start = (-.4, 0, -.2 + (ord(start) - ord('1')) * .2)
        end = (.1, 0, -.2 + (ord(end) - ord('A')) * .2)

        vertices = [
            (start[0], start[1] - side, start[2] - side),
            (end[0], end[1] - side, end[2] - side),
            (end[0], end[1] + side, end[2] - side),
            (start[0], start[1] + side, start[2] - side),
            (start[0], start[1] - side, start[2] + side),
            (end[0], end[1] - side, end[2] + side),
            (end[0], end[1] + side, end[2] + side),
            (start[0], start[1] + side, start[2] + side),
        ]
        box = create_box_from_vertices(vertices)
        return HitBox((0, 0, 0), box)

    def _update_stage(self):
        # update wire colors
        for index, config in enumerate(self._model.wires):
            if not config:
                continue

            wire_name = 'Wire{0}{1}{3}_wire{0}{2}{3}'.format(
                index + 1,
                config[1],
                config[1].lower(),
                'cut' if config[2] else '')

            set_material_color(self._wires.meshes[wire_name], colors[config[0]])

        # update LEDs
        on = (1, 0, 0, 1)
        off = (.2, 0, 0, 1)
        for i in range(4):
            self._leds[i].color = on if i < self._model.changes else off
            self._leds[i].active = i < self._model.changes

    def _animate_change(self, action):
        self._door_closed_action = action

    def _on_wrong_try(self):
        self._alert += .5
        set_material_color(self._static.materials['Font'], (1, 0, 0, 1), emissive=(1, 0, 0, 1))

    def _tick(self, dt):
        if self._alert > 0:
            self._alert = max(self._alert - dt, 0)
            if self._alert <= 0:
                set_material_color(self._static.materials['Font'], (1, 1, 0, 1), emissive=(1, 1, 0, 1))

        if self._door_closed_action:
            self._door_scale += dt * 2
            if self._door_scale >= 1.3:
                self._door_closed_action()
                self._door_closed_action = None
                self._update_stage()

        if not self._door_closed_action and self._door_scale > .05:
            self._door_scale = max(self._door_scale - dt * 2, .05)


class ComplicatedWire(Module):
    def __init__(self, model):
        super().__init__(model, static='resources/complicated_wire.obj')

        self._set_colors()
        self._update_wires()

        self._wire_boxes = [create_box(*get_bounds(self._static.meshes['Wire{0}_wire{0}'.format(index + 1)]))
                            for index in range(6)]
        self._alert = 0
        self._model.on_wrong_try += self._on_wrong_try
        clock.schedule_interval(self._tick, .01)

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        hit_distance = None
        hit_index = None

        for index, config in enumerate(self._model.wires):
            if not config or config[3]:
                continue

            distance = get_hit_distance(self._wire_boxes[index], position, self._model_matrix)
            if distance is not None:
                if not hit_index or distance < hit_distance:
                    hit_distance = distance
                    hit_index = index

        return self if hit_distance else None, hit_index, hit_distance

    def hit(self, tag, button, modifiers):
        self._model.cut(tag)
        self._update_wires()

    def _update_wires(self):
        for index, wire in enumerate(self._model.wires):
            if wire[0] and not wire[3]:
                continue

            tag = 'Wire{0}_wire{0}'.format(index + 1)
            if tag not in self._static.meshes:
                continue

            mesh = self._static.meshes[tag]
            if mesh not in self._static.mesh_list:
                continue

            self._static.mesh_list.remove(self._static.meshes[tag])

    def _set_colors(self):
        for index, wire in enumerate(self._model.wires):
            set_material_color(self._static.materials['LED{}'.format(index + 1)],
                               colors['Green'] if wire[1] else colors['Black'],
                               emissive=colors['Green'] if wire[1] else None)
            set_material_color(self._static.materials['X{}'.format(index + 1)],
                               colors['Green'] if wire[2] else colors['Black'],
                               emissive=colors['Green'] if wire[2] else None)

            # do not set color for cut wires or not existing wires
            if not wire[0] or wire[3]:
                continue

            for i, name in enumerate(['A', 'B']):
                set_material_color(self._static.materials['Wire{}{}'.format(index + 1, name)],
                                   colors[wire[0][i % len(wire[0])]])

    def _on_wrong_try(self):
        self._alert += .5
        for index in range(6):
            set_material_color(self._static.materials['LED{}'.format(index + 1)], colors['Red'], emissive=colors['Red'])
            set_material_color(self._static.materials['X{}'.format(index + 1)], colors['Red'], emissive=colors['Red'])

    def _tick(self, dt):
        if self._alert > 0:
            self._alert = max(self._alert - dt, 0)
            if self._alert <= 0:
                self._set_colors()
