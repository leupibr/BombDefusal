from datetime import timedelta

import pytest
from mock import mock

import bomb_defusal.modules.knobs
import bomb_defusal.modules.module
from bomb_defusal.modules import Knobs, KnobOrientation
from bomb_defusal.modules.knobs import LedSet
from tests.utils.controllable_clock import ControllableClock


@pytest.fixture(autouse=True)
def clock(monkeypatch):
    clock_ = ControllableClock()
    monkeypatch.setattr(bomb_defusal.modules.module, 'clock', clock_)
    return clock_


class TestKnobs(object):
    def test_invalid_position(self, clock):
        subject = Knobs(None, timedelta(seconds=2))
        subject.on_wrong_try = mock.MagicMock()
        subject.activate()
        if subject.leds in {LedSet.UpOne.value, LedSet.UpTwo.value}:
            subject.orientation = KnobOrientation.Left
        else:
            subject.orientation = KnobOrientation.Up
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)

        subject.on_wrong_try.assert_called_once()

    def test_correct_position(self, clock):
        subject = Knobs(None, timedelta(seconds=2))
        subject.on_wrong_try = mock.MagicMock()
        subject.activate()
        if subject.leds in {LedSet.RightOne.value, LedSet.RightTwo.value}:
            subject.orientation = KnobOrientation.Right
        if subject.leds in {LedSet.UpOne.value, LedSet.UpTwo.value}:
            subject.orientation = KnobOrientation.Up
        if subject.leds in {LedSet.LeftOne.value, LedSet.LeftTwo.value}:
            subject.orientation = KnobOrientation.Left
        if subject.leds in {LedSet.DownOne.value, LedSet.DownTwo.value}:
            subject.orientation = KnobOrientation.Down
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)

        subject.on_wrong_try.assert_not_called()
        assert timedelta(seconds=2) == subject.expiration
