import random

from bomb_defusal.modules.module import DisarmableModule


class Memory(DisarmableModule):
    """
    On the memory module, you have to press the right buttons in the right order, depending on the label.
    You always should remember your old action as they might have impact on newer ones.
    """

    def __init__(self, position, disarmed, labels):
        """
        Initializes a new instance of the :class:`Memory` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Flag that indicates the initial disarmed status of the module.
        :param list[str] labels: Labels to display

        :raises ValueError: If one of the attributes is invalid.
        """
        super().__init__(position, disarmed)

        if len(labels) != 5:
            raise ValueError('There must be exactly five buttons to press.')

        if not all(i in '1234' for i in labels):
            raise ValueError('Invalid label')

        self._labels = labels
        self._label, self._buttons = self._next_stage()
        self._history = []

    @property
    def buttons(self):
        return self._buttons

    @property
    def disarm_stage(self):
        """
        Gets the actual disarm stage from 0 (no action yet) to 5 (disarmed).

        :rtype: int
        """
        return 5 if self._disarmed else 4 - len(self._labels)

    @property
    def label(self):
        return self._label

    def press(self, position):
        """
        Press the button at the given position.

        :param int position: Zero based index of the button to press.
        """
        assert position in range(0, 4)

        if position != self._expected_position():
            self.on_wrong_try()
            return

        self._history.append((position, self.buttons[position]))

        if not self._labels:
            self._disarmed = True
            return
        self._label, self._buttons = self._next_stage()

    def _expected_position(self):
        stage = self.disarm_stage
        label = int(self._label) - 1
        if stage == 0:
            return [1,1,2,3][label]
        if stage == 1:
            return {
                0: self.buttons.index('4'),
                1: self._get_history_position(0),
                2: 0,
                3: self._get_history_position(0)
            }[label]
        if stage == 2:
            return {
                0: self._get_history_label_pos(1),
                1: self._get_history_label_pos(0),
                2: 2,
                3: self.buttons.index('4')
            }[label]
        if stage == 3:
            return {
                0: self._get_history_position(0),
                1: 0,
                2: self._get_history_position(1),
                3: self._get_history_position(1)
            }[label]

        return [self._get_history_label_pos(i) for i in [0,1,3,2]][label]

    def _get_history_position(self, stage):
        return self._history[stage][0]

    def _get_history_label_pos(self, stage):
        label = self._history[stage][1]
        return self.buttons.index(label)

    def _next_stage(self):
        """Loads the next stage"""
        label = self._labels.pop(0)
        buttons = [str(i) for i in range(1,5)]
        random.shuffle(buttons)
        return label, buttons
