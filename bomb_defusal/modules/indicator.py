from enum import Enum

from bomb_defusal.modules.module import Module


class IndicatorText(Enum):
    SND = 'SND'
    CLR = 'CLR'
    CAR = 'CAR'
    IND = 'IND'
    FRQ = 'FRQ'
    SIG = 'SIG'
    NSA = 'NSA'
    MSA = 'MSA'
    TRN = 'TRN'
    BOB = 'BOB'
    FRK = 'FRK'


class Indicator(Module):
    """This module is a placeholder for labels places on the bomb."""
    def __init__(self, position, label):
        """
        Initializes a new instance of the :class:`Keypad` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param IndicatorText label: Label to display.

        :raises ValueError: If one of the attributes is invalid.
        """
        super().__init__(position)

        if isinstance(label, str):
            label = IndicatorText[label]

        if not isinstance(label, IndicatorText):
            raise ValueError('Invalid label')

        self._label = label

    @property
    def label(self):
        """
        Label of the indicator

        :rtype: IndicatorText
        """
        return self._label

    def __str__(self):
        return self._label.name
