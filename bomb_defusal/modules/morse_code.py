from bomb_defusal.modules.module import DisarmableModule


class MorseCode(DisarmableModule):
    """
    This module sends a Morse code of a given word.
    Based on this word a correct frequency must be looked up and locked.
    """
    _alphabet = {
        'A': '.−',
        'B': '−...',
        'C': '−.−.',
        'D': '−..',
        'E': '.',
        'F': '..−.',
        'G': '−−.',
        'H': '....',
        'I': '..',
        'J': '.−−−',
        'K': '−.−',
        'L': '.−..',
        'M': '−−',
        'N': '−.',
        'O': '−−−',
        'P': '.−−.',
        'Q': '−−.−',
        'R': '.−.',
        'S': '...',
        'T': '−',
        'U': '..−',
        'V': '...−',
        'W': '.−−',
        'X': '−..−',
        'Y': '−.−−',
        'Z': '−−..',
        '1': '.−−−−',
        '2': '..−−−',
        '3': '...−−',
        '4': '....−',
        '5': '.....',
        '6': '−....',
        '7': '−−...',
        '8': '−−−..',
        '9': '−−−−.',
        '0': '−−−−−',
    }

    _frequency_map = {
        'shell': '3.505 MHz',
        'halls': '3.515 MHz',
        'slick': '3.522 MHz',
        'trick': '3.532 MHz',
        'boxes': '3.535 MHz',
        'leaks': '3.542 MHz',
        'strobe': '3.545 MHz',
        'bistro': '3.552 MHz',
        'flick': '3.555 MHz',
        'bombs': '3.565 MHz',
        'break': '3.572 MHz',
        'brick': '3.575 MHz',
        'steak': '3.582 MHz',
        'sting': '3.592 MHz',
        'vector': '3.595 MHz',
        'beats': '3.600 MHz'
    }

    def __init__(self, position, disarmed, word):
        """
        Initializes a new instance of the :class:`MorseCode` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Flag that indicates the initial disarmed status of the module.
        :param str word: Word that is morsed.

        :raises ValueError: If one of the attributes is invalid.
        """
        super().__init__(position, disarmed)

        if word not in MorseCode._frequency_map:
            raise ValueError('Invalid word')

        self._word = word
        self._index = len(self._frequency_map) // 2

    @property
    def word(self):
        """
        The word that is morsed

        :rtype: str
        """
        return self._word

    @property
    def index(self):
        return self._index

    @property
    def frequency(self):
        return sorted(self._frequency_map.values())[self._index]

    @property
    def frequencies(self):
        """
        Gets the supported frequencies of the morse code module.

        :return: Supported frequencies
        :rtype: list[str]
        """
        return list(sorted(MorseCode._frequency_map.values()))

    @property
    def code(self):
        """
        The word as a morse code array ('.', '-')

        :rtype: list[str]
        """
        return [MorseCode._alphabet[c] for c in self._word.upper()]

    def next(self):
        if self.disarmed:
            return

        if self._index != len(self._frequency_map) - 1:
            self._index += 1

    def previous(self):
        if self.disarmed:
            return

        if self._index != 0:
            self._index -= 1

    def transmit(self):
        """
        Lock in the current frequency frequency.
        """
        assert self.frequency in MorseCode._frequency_map.values()

        expected = MorseCode._frequency_map[self._word]
        if self.frequency != expected:
            self.on_wrong_try()
            return

        self._disarmed = True
