import pytest
from mock import mock

from bomb_defusal.modules.keypad import Keypad, KeypadChar


class TestKeypad(object):
    @staticmethod
    def _assert_disarm_success(subject, order):
        subject.on_wrong_try = mock.MagicMock()
        [subject.press(b) for b in order]
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, order):
        subject.on_wrong_try = mock.MagicMock()
        [subject.press(b) for b in order]
        assert not subject.disarmed
        subject.on_wrong_try.assert_called()

    @pytest.mark.parametrize('buttons,order', [
        ([KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, KeypadChar.ARCHKOPPA, KeypadChar.YUS], [2, 3, 0, 1]),
    ])
    def test_press_correct_order(self, buttons, order):
        subject = Keypad(None, False, buttons)
        self._assert_disarm_success(subject, [buttons[i] for i in order])

    @pytest.mark.parametrize('buttons,order', [
        ([KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, KeypadChar.ARCHKOPPA, KeypadChar.YUS], [2, 3, 1, 0]),
    ])
    def test_press_invalid_order(self, buttons, order):
        subject = Keypad(None, False, buttons)
        self._assert_disarm_failure(subject, [buttons[i] for i in order])

    @pytest.mark.parametrize('buttons', [
        ([KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, 'INVALID', KeypadChar.YUS]),
        ([KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, KeypadChar.YUS]),
        ([KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, KeypadChar.ARCHKOPPA, KeypadChar.YUS, KeypadChar.IBIGYUS]),
        ([KeypadChar.IBIGYUS, KeypadChar.PHI, KeypadChar.ARCHKOPPA, KeypadChar.YUS]),
    ])
    def test_press_invalid_buttons(self, buttons):
        with pytest.raises(ValueError):
            Keypad(None, False, buttons)

    def test_buttons(self):
        buttons = [KeypadChar.IBIGYUS, KeypadChar.RSIGMADOT, KeypadChar.ARCHKOPPA, KeypadChar.YUS]
        assert buttons == Keypad(None, False, buttons[:]).buttons
