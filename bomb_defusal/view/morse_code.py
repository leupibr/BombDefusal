from pyglet import clock
from pyglet.gl import glPushMatrix, glPopMatrix, glTranslatef

from bomb_defusal.view.components import Button, Static, Text, Led
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import colors


class MorseCode(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`MorseCode` class.

        :param bomb_defusal.model.MorseCode model: Model of the morse code module
        """
        super().__init__(model, static='resources/morse_code.obj')

        self._down = Button((-.375, .04, .1), static='resources/components/morse_button.obj')
        self._up = Button((.375, .04, .1), static='resources/components/morse_button.obj', rotate_y=180)
        self._transmit = Button((0, 0, .45), label='TX')
        self._needle = Static((-.375, 0, -.15), static='resources/components/morse_needle.obj')
        self._led = Led((-.375, 0, -.5), scale=(1.5, 1, 1.5), color=(.2, .2, .2, 1), active_color=(.8, .8, .8, 1))
        self._frequency = Text((0, -.085, .1), text_height=.1, text_font='resources/fonts/sans.ttf',
                               foreground=(0, 200, 0))

        self._frequency.text = self._model.frequency

        self._tick_count = 0
        self._led_states = list(self._get_led_states(self._model.code))

        clock.schedule_interval(self._tick, .3)

    def hit_test(self, position, button, modifiers):
        """Tests the buttons of the keypad module for hits by the mouse"""
        if button != 1:
            return None, None, None

        for name, button in [('up', self._up), ('down', self._down), ('tx', self._transmit)]:
            distance = button.hit_test(*position)
            if distance:
                return self, name, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        """Performs the hit action on the selected button"""
        if tag == 'up':
            self._up.press()
            self._model.next()
        elif tag == 'down':
            self._down.press()
            self._model.previous()
        elif tag == 'tx':
            self._transmit.press()
            self._model.transmit()
            if self._model.disarmed:
                clock.unschedule(self._tick)
                self._led.active = False
            return

        self._frequency.text = self._model.frequency

    def hit_release(self, tag, button, modifiers):
        """Releases any pressed button"""
        if tag == 'up':
            self._up.release()
        elif tag == 'down':
            self._down.release()
        elif tag == 'tx':
            self._transmit.release()

    def on_draw(self):
        self._down.draw()
        self._up.draw()
        self._transmit.draw()
        self._frequency.draw()
        self._led.draw()
        glPushMatrix()
        glTranslatef(self._model.index * (.75 / (len(self._model.frequencies) - 1)), 0, 0)
        self._needle.draw()
        glPopMatrix()

    def _get_led_states(self, code):
        for morse in code:
            for state in morse:
                for _ in range(1 if state == '.' else 5):
                    yield True
                yield False
            for _ in range(5):
                yield False

    def _tick(self, dt):
        self._led.active = self._led_states[self._tick_count % len(self._led_states)]
        self._tick_count += 1
