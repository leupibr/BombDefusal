import threading

import datetime
from PIL import Image, ImageDraw, ImageFont
from pyglet import clock
from pyglet.gl import *
from pyglet.image import ImageData, Texture, load as load_image

from bomb_defusal import audio
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import vector


class CountdownTimer(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`CountdownTimer` class.

        :param bomb_defusal.modules.CountdownTimer model: Model of the timer module
        """
        super().__init__(model, static='resources/timer.obj')
        self._font = ImageFont.truetype(font='resources/fonts/ticking.ttf', size=128)
        self._skull = load_image('resources/strike.png').get_texture()
        self._timer = None
        self._seconds = None
        clock.schedule(self._update_timer, .1)

        self._stop = threading.Event()
        thread = threading.Thread(target=model.run, args=(self._stop,))
        thread.daemon = True
        thread.start()

    def on_draw(self):
        """Draw the 3D view of the module"""
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, vector(0, 0, 0, 1))
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, vector(0, 0, 0, 1))
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, vector(0, 0, 0, 1))
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, vector(1, 0, 0, 1))

        self._draw_clock()
        self._draw_strikes()

    def _update_timer(self, *args, **kwargs):
        """Update the clock texture for later rendering"""
        time_left = int(self._model.time_left.total_seconds())
        if time_left <= 0:
            self._stop.set()
            clock.unschedule(self._update_timer)
            return

        if self._seconds == time_left:
            return

        self._timer = self._draw_time()
        self._seconds = time_left
        if time_left % 5 == 0:
            audio.play('beep', volume=.05)

    def _draw_clock(self):
        """Draws the current remaining time on top of the 3D LCD display"""
        if not self._timer:
            return

        glPushMatrix()
        glScalef(.8, 1, .4)
        glTranslatef(-.5, 0, .5)

        glEnable(self._timer.target)
        glBindTexture(self._timer.target, self._timer.id)
        glBegin(GL_QUADS)

        vertices = [(0, 0, 0), (0, 0, 1), (1, 0, 1), (1, 0, 0)]
        for i in range(4):
            glTexCoord2f(*vertices[i][::2])
            glVertex3f(*vertices[i])

        glEnd()
        glDisable(self._timer.target)
        glPopMatrix()

    def _draw_strikes(self):
        """Draw the skulls for the strikes"""
        glPushMatrix()
        glScalef(.15, 1, .15)
        glTranslatef(-2.5, 0, -2.35)

        glEnable(self._skull.target)
        glBindTexture(self._skull.target, self._skull.id)
        glBegin(GL_QUADS)

        vertices = [(0, 0, 0), (0, 0, 1), (1, 0, 1), (1, 0, 0)]
        for strike in range(min(2, self._model.strikes)):
            for i in range(4):
                glTexCoord2f(*vertices[i][::2])
                glVertex3f(vertices[i][0] + (strike * 1.1), vertices[i][1], vertices[i][2])

        glEnd()
        glDisable(self._skull.target)
        glPopMatrix()

    def _draw_time(self):
        """Create the texture containing the current remaining time of the timer"""
        display = str(self._model.time_left)

        text_size = self._font.getsize(display)
        size = [1 << (x - 1).bit_length() for x in text_size]

        image = Image.new('RGBA', size, (0, 0, 0, 0))
        draw = ImageDraw.Draw(image)
        draw.text((50, 0), display, (255, 255, 255), font=self._font)
        data = ImageData(*size, 'RGBA', image.tobytes(), pitch=size[0] * 4)

        texture = Texture.create(width=size[0], height=size[1])
        texture.blit_into(data, x=0, y=0, z=0)
        return texture
