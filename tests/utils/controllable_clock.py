class ControllableClock(object):
    def __init__(self):
        self._onces = []
        self._intervals = []

    @property
    def once_schedules(self):
        return self._onces

    @property
    def interval_schedules(self):
        return self._intervals

    def schedule_once(self, func, delay, *args, **kwargs):
        self._onces.append((func, args, kwargs))

    def schedule_interval(self, func, delay, *args, **kwargs):
        self._intervals.append((func, args, kwargs))

    def fire_once_schedule(self, index, pop=True):
        func, args, kwargs = self._onces[index] if not pop else self._onces.pop(index)
        func(*args, **kwargs)

    def fire_interval_schedule(self, index):
        func, args, kwargs = self._intervals[index]
        func(*args, **kwargs)
