import pytest

from bomb_defusal.modules import Battery


class TestBattery(object):
    @pytest.mark.parametrize('num_units', range(1, 3))
    def test_unit(self, num_units):
        assert num_units == Battery(None, num_units).units

    @pytest.mark.parametrize('num_units', [0, 3])
    def test_invalid_number_of_units(self, num_units):
        with pytest.raises(ValueError):
            assert num_units == Battery(None, num_units).units
