import pytest
from mock import mock

from bomb_defusal.modules.who_is_on_first import WhoIsOnFirst


class TestWhoIsOnFirst(object):
    @staticmethod
    def _assert_disarm_success(subject, buttons):
        subject.on_wrong_try = mock.MagicMock()

        for b in buttons:
            assert not subject.disarmed
            subject.press(b)

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, buttons):
        subject.on_wrong_try = mock.MagicMock()

        for b in buttons:
            assert not subject.disarmed
            subject.press(b)

        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

    @pytest.mark.parametrize('buttons', [
        (['NO', 'LEFT', 'NOTHING', 'WAIT']),
        (['LIKE', 'LIKE', 'LIKE', 'LIKE'])
    ])
    def test_correct_order(self, buttons):
        subject = WhoIsOnFirst(None, False, buttons[:])
        self._assert_disarm_success(subject, buttons[:])

    @pytest.mark.parametrize('invalid_position', range(3))
    def test_invalid_press(self, invalid_position):
        buttons = ['NO', 'LEFT', 'NOTHING', 'WAIT']
        subject = WhoIsOnFirst(None, False, buttons[:])
        buttons[invalid_position] = 'WRONG'
        buttons = buttons[:invalid_position+1]
        self._assert_disarm_failure(subject, buttons[:])

    @pytest.mark.parametrize('buttons', [
        (['ONE', 'TWO', 'THREE']),
        (['INVALID', 'LEFT', 'NOTHING', 'WAIT'])
    ])
    def test_invalid_construction(self, buttons):
        with pytest.raises(ValueError):
            WhoIsOnFirst(None, False, buttons[:])

    def test_label(self):
        subject = WhoIsOnFirst(None, False, ['NO', 'LEFT', 'NOTHING', 'WAIT'][:])
        assert isinstance(subject.label, str)

    def test_buttons(self):
        buttons = WhoIsOnFirst(None, False, ['NO', 'LEFT', 'NOTHING', 'WAIT']).buttons
        assert len(buttons) == 6

    def test_disarm_stage(self):
        subject = WhoIsOnFirst(None, False, ['NO', 'LEFT', 'NOTHING', 'WAIT'])
        assert 0 == subject.disarm_stage
        subject.press('NO')
        assert 1 == subject.disarm_stage
        subject.press('LEFT')
        assert 2 == subject.disarm_stage
        subject.press('NOTHING')
        assert 3 == subject.disarm_stage
        subject.press('WAIT')
        assert 4 == subject.disarm_stage
        assert subject.disarmed
