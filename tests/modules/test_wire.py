import pytest
from mock import mock

from bomb_defusal.modules import Wire, ComplicatedWire, WireSequence, PortType


class TestWire(object):
    @staticmethod
    def _assert_disarm_success(subject, cut):
        subject.on_wrong_try = mock.MagicMock()
        subject.cut(cut)

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, cut):
        subject.on_wrong_try = mock.MagicMock()
        subject.cut(cut)

        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

    @pytest.mark.parametrize('wires', [[], ['Blue'] * 1, ['Blue'] * 2, ['Blue'] * 7, ['Blue'] * 8])
    def test_init_invalid_num_of_wires(self, wires):
        with pytest.raises(ValueError):
            Wire(None, False, wires, None)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Blue', 'Green', 'Black'], 2, None),
        (['Blue', 'Red', 'White'], 3, None),
        (['Blue', 'Red', 'Blue'], 3, None),
        (['Blue', 'Blue', 'Red'], 2, None),
        (['Red', 'Blue', 'Blue'], 3, None),
        (['Red', 'White', 'Black'], 3, None),
    ])
    def test_three_wire_success(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_success(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Red', 'Red', 'Blue', 'Green'], 2, True),
        (['Red', 'Red', 'Red', 'Green'], 3, True),
        (['Red', 'Green', 'Blue', 'Red'], 4, True),
        (['Blue', 'Green', 'Blue', 'Yellow'], 1, None),
        (['Yellow', 'Green', 'Blue', 'Red'], 1, None),
        (['Yellow', 'Green', 'Yellow', 'Red'], 4, None),
        (['Green'] * 4, 2, None),
        (['Red', 'Red', 'Blue', 'Blue'], 2, False),
        (['Red', 'Red', 'Red', 'Green'], 2, False),
        (['Red', 'Green', 'Black', 'Red'], 2, False)
    ])
    def test_four_wire_success(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_success(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Blue', 'Green', 'Red', 'Yellow', 'Black'], 4, True),
        (['Green', 'Yellow', 'Black', 'White', 'Black'], 4, True),
        (['Red', 'Yellow', 'Black', 'Yellow', 'Blue'], 1, None),
        (['Yellow', 'Yellow', 'Black', 'Yellow', 'Red'], 1, None),
        (['Yellow', 'White', 'Blue', 'Yellow', 'Yellow'], 2, None),
        (['Black', 'Green', 'Green', 'Green', 'Green'], 1, None),
        (['Green', 'Yellow', 'Black', 'White', 'Green'], 1, True),
    ])
    def test_five_wire_success(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_success(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Green'] * 6, 3, True),
        (['Blue'] * 6, 3, True),
        (['Blue', 'Green', 'White', 'Green', 'Red', 'Blue'], 3, True),
        (['White', 'Yellow', 'White', 'Green', 'Red', 'Blue'], 4, None),
        (['White', 'Yellow', 'Black', 'Green', 'Black', 'Blue'], 6, None),
        (['Yellow', 'Yellow', 'Red', 'Red', 'Black', 'Black'], 4, None),
    ])
    def test_six_wire_success(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_success(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Blue', 'Green', 'Black'], 1, None),
        (['Blue', 'Red', 'White'], 2, None),
        (['Blue', 'Red', 'Blue'], 2, None),
        (['Blue', 'Blue', 'Red'], 1, None),
        (['Red', 'Blue', 'Blue'], 2, None),
        (['Red', 'White', 'Black'], 1, None),
    ])
    def test_three_wire_failure(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_failure(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Red', 'Red', 'Blue', 'Green'], 1, True),
        (['Red', 'Red', 'Red', 'Green'], 4, True),
        (['Red', 'Green', 'Blue', 'Red'], 3, True),
        (['Blue', 'Green', 'Blue', 'Yellow'], 2, None),
        (['Yellow', 'Green', 'Blue', 'Red'], 3, None),
        (['Yellow', 'Green', 'Yellow', 'Red'], 1, None),
        (['Green'] * 4, 4, None),
        (['Red', 'Red', 'Blue', 'Blue'], 1, False),
        (['Red', 'Red', 'Red', 'Green'], 3, False),
        (['Red', 'Green', 'Black', 'Red'], 4, False)
    ])
    def test_four_wire_failure(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_failure(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Blue', 'Green', 'Red', 'Yellow', 'Black'], 5, True),
        (['Green', 'Yellow', 'Black', 'White', 'Black'], 3, True),
        (['Red', 'Yellow', 'Black', 'Yellow', 'Blue'], 2, None),
        (['Yellow', 'Yellow', 'Black', 'Yellow', 'Red'], 4, None),
        (['Yellow', 'White', 'Blue', 'Yellow', 'Yellow'], 1, None),
        (['Black', 'Green', 'Green', 'Green', 'Green'], 5, None),
        (['Green', 'Yellow', 'Black', 'White', 'Green'], 2, True),
    ])
    def test_five_wire_failure(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_failure(subject, cut)

    @pytest.mark.parametrize('wires,cut,is_odd', [
        (['Green'] * 6, 2, True),
        (['Blue'] * 6, 1, True),
        (['Blue', 'Green', 'White', 'Green', 'Red', 'Blue'], 4, True),
        (['White', 'Yellow', 'White', 'Green', 'Red', 'Blue'], 6, None),
        (['White', 'Yellow', 'Black', 'Green', 'Black', 'Blue'], 5, None),
        (['Yellow', 'Yellow', 'Red', 'Red', 'Black', 'Black'], 3, None),
    ])
    def test_six_wire_failure(self, wires, cut, is_odd):
        bomb = mock.MagicMock()
        bomb.serial_number.is_odd = is_odd
        bomb.serial_number.is_even = not is_odd
        subject = Wire(None, False, wires, bomb)
        self._assert_disarm_failure(subject, cut)


class TestComplicatedWire(object):
    def test_wires(self):
        wires = [
            (('Red', 'Blue'), False, False),
            (('Red'), True, False),
            (('Yellow'), True, True),
            (('Black'), False, False),
            (('Green'), False, True),
            (('Blue', 'White'), False, True)]
        subject = ComplicatedWire(None, False, wires, None)
        for i in range(0, 6):
            assert subject.wires[i] == wires[i] + (False,)

    def test_empty_positions(self):
        bomb = mock.MagicMock()
        bomb.serial_number.is_even = True
        bomb.serial_number.is_odd = False
        bomb.ports = [PortType.PARALLEL]
        bomb.battery_count = 4
        wires = [(('Blue',), False, True),
                 (None, False, False),  # "Non-Wire"
                 (('Black', 'Red'), False, True),
                 (('Red', 'Blue'), True, False),
                 (('Red', 'Blue'), False, False),
                 (('Red', 'Blue'), True, False)]
        subject = ComplicatedWire(None, False, wires, bomb)
        subject.cut(0)
        subject.cut(1)
        subject.cut(2)

    @pytest.mark.parametrize('wires', [
        [(('Red', 'Blue'), False, False)],
        [(('Red', 'Blue'), False, False)] * 7,
    ])
    def test_invalid_wires(self, wires):
        with pytest.raises(ValueError):
            ComplicatedWire(None, False, wires, None)

    @pytest.mark.parametrize('wires,cut', [
        ([
             (('Yellow',), True, False),  # C1 Type
             (('Blue',), True, False),  # D1 Type
             (('Red',), False, False),  # S1 Type
             (('Red', 'Blue'), True, False),  # P1 Type
             (('Black',), False, False),  # B1 Type
             (('Red',), True, False),  # C2 Type
         ], [0, 2, 3, 4, 5]),
        ([
             (('Black',), False, False),  # C3 Type
             (('Red', 'Blue'), True, True),  # D2 Type
             (('Black',), False, True),  # D3 Type
             (('Red', 'Blue'), False, True),  # S2 Type
             (('Blue',), False, False),  # S3 Type
             (('Blue',), True, True),  # P2 Type
         ], [0, 3, 4, 5]),
        ([
             (('Blue',), False, True),  # P3 Type
             (('Red', 'Yellow'), True, True),  # B2 Type
             (('Black', 'Red'), False, True),  # B3 Type
             (('Red', 'Blue'), True, True),  # D2 Type
             (('Red', 'Blue'), True, True),  # D2 Type
             (('Red', 'Blue'), True, True),  # D2 Type
         ], [0, 1, 2])
    ])
    def test_disarm(self, wires, cut):
        bomb = mock.MagicMock()
        bomb.serial_number.is_even = True
        bomb.serial_number.is_odd = False
        bomb.ports = [PortType.PARALLEL]
        bomb.battery_count = 4
        subject = ComplicatedWire(None, False, wires, bomb)
        subject.on_wrong_try = mock.MagicMock()
        [subject.cut(i) for i in cut]

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @pytest.mark.parametrize('wires', [[(('Yellow',), True, False)] * 6])
    def test_double_cut(self, wires):
        bomb = mock.MagicMock()
        bomb.serial_number.is_even = True
        bomb.serial_number.is_odd = False
        bomb.ports = [PortType.PARALLEL]
        bomb.battery_count = 4
        subject = ComplicatedWire(None, False, wires, bomb)
        subject.on_wrong_try = mock.MagicMock()
        # cut one
        [subject.cut(i) for i in range(6)]
        # cut two
        [subject.cut(i) for i in range(6)]

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @pytest.mark.parametrize('wires', [[(('Red', 'Blue'), True, True)] * 6])
    def test_invalid_try(self, wires):
        bomb = mock.MagicMock()
        bomb.serial_number.is_even = True
        bomb.serial_number.is_odd = False
        bomb.ports = [PortType.PARALLEL]
        bomb.battery_count = 4
        subject = ComplicatedWire(None, False, wires, bomb)
        subject.on_wrong_try = mock.MagicMock()

        subject.cut(0)
        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()


class TestWireSequence(object):
    def test_wires(self):
        wires = [
            ('Red', 'A'), ('Red', 'B'), ('Black', 'B'),
            ('Blue', 'B'), None, ('Black', 'C'),
            None, None, ('Blue', 'A'),
        ]
        subject = WireSequence(None, False, wires[:])
        wires = [(*w, False) if w else None for w in wires]

        for i in [0, 3, 6]:
            wires[i:i + 3] = subject.wires
            subject.next()
        for i in [6, 3, 0]:
            wires[i:i + 3] = subject.wires
            subject.previous()
        wires[0:3] = subject.wires

    def test_changes(self):
        wires = [
            ('Red', 'A'), ('Red', 'B'), ('Black', 'B'),
            ('Blue', 'B'), None, ('Black', 'C'),
            None, None, ('Blue', 'A'),
        ]
        subject = WireSequence(None, False, wires[:])
        subject.on_wrong_try = mock.MagicMock()
        assert subject.changes == 0
        subject.next()
        assert subject.changes == 1
        subject.next()
        assert subject.changes == 2
        subject.previous()
        assert subject.changes == 3
        subject.next()
        assert subject.changes == 4
        subject.next()
        assert subject.changes == 4
        subject.on_wrong_try.assert_called_once()


    @pytest.mark.parametrize('wires', [
        [('Yellow', 'A')] * 9,
        [('Red', 'F')] * 9,
        [('Red', 'A')] * 7,
        [('Red', 'A')] * 10,
    ])
    def test_invalid_wires(self, wires):
        with pytest.raises(ValueError):
            WireSequence(None, False, wires)

    @pytest.mark.parametrize('wires', [[
        ('Red', 'A'), None, ('Black', 'C'),
        ('Black', 'B'), ('Red', 'B'), ('Blue', 'B'),
        None, None, ('Blue', 'A')]])
    def test_disarm(self, wires):
        subject = WireSequence(None, False, wires)
        subject.on_wrong_try = mock.MagicMock()
        subject.cut(2)
        subject.next()
        subject.cut(1)
        subject.cut(2)
        subject.next()
        subject.cut(2)
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @pytest.mark.parametrize('wires', [[
        ('Red', 'A'), None, ('Black', 'C'),
        ('Black', 'B'), ('Red', 'B'), ('Blue', 'B'),
        None, None, ('Blue', 'A')]])
    def test_wrong_cut(self, wires):
        subject = WireSequence(None, False, wires)
        subject.on_wrong_try = mock.MagicMock()
        subject.cut(0)
        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

