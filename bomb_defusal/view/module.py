from pyglet import clock
from pyglet.gl import *

from bomb_defusal.modules.module import DisarmableModule
from bomb_defusal.view.utils.drawing import set_material_color
from bomb_defusal.view.utils.helper import load_model, matrix4x4


class Module(object):
    Front = 'Front'
    Rear = 'Rear'
    Left = 'Left'
    Right = 'Right'
    Top = 'Top'
    Bottom = 'Bottom'

    status_led = {
        False: [([.1, .0, .0, 1], [.1, .0, .0, 1], [.4, .1, .1, 1], [.1, .0, .0, 1]),
                ([.7, .0, .0, 1], [.7, .0, .0, 1], [.9, .5, .5, 1], [.7, .0, .0, 1])],
        True:  [([.0, .3, .0, 1], [.0, .3, .0, 1], [.2, .5, .2, 1], [.0, .5, .0, 1])]
    }

    def __init__(self, model, **kwargs):
        """
        Initializes a new instance of the :class:`Module` class.

        :param bomb_defusal.modules.module.Module model: Model of the module
        """
        self._model = model
        self._model_matrix = matrix4x4()

        self._base = \
            load_model('resources/base.obj') if 'base' not in kwargs else \
            load_model(kwargs.get('base')) if kwargs.get('base') else None
        self._static = load_model(kwargs.get('static')) if kwargs.get('static') else None
        self._status = None

        if isinstance(model, DisarmableModule):
            self._status = load_model('resources/components/status.obj')
            self._elapsed = 0
            clock.schedule_interval(self._update_status, .1)

    def draw(self):
        """Draws the 3D view of the module"""
        glPushMatrix()
        glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT)
        glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT)

        self._move_to_slot()
        glGetDoublev(GL_MODELVIEW_MATRIX, self._model_matrix)

        if self._base:
            self._base.draw()
        if self._status:
            self._status.draw()
        if self._static:
            self._static.draw()
        self.on_draw()

        glPopAttrib()
        glPopClientAttrib()
        glPopMatrix()

    def on_draw(self):
        """Allows to add custom 3D elements to derived modules"""
        pass

    def hit_test(self, position, button, modifiers):
        """Allows to test for mouse hits in derived modules"""
        return None, None, None

    def hit(self, tag, button, modifiers):
        """Performs custom hit actions in derived modules"""
        pass

    def hit_release(self, tag, button, modifiers):
        """Allows to handle mouse button release actions in derived modules"""
        pass

    def _move_to_slot(self):
        """Translate and rotate the 3D view so it appears in the correct position on the bomb case"""
        side, index = self._model.position
        slots_per_line = {Module.Right: 2, Module.Left: 2}.get(side, 3)
        x, y = index % slots_per_line, index // slots_per_line

        if side in [Module.Right, Module.Left]:
            if side == Module.Left:
                glTranslatef(-3, 1 - (x * 2), (y - .5) * .75)
                glRotatef(90, 0, 0, 1)
            else:
                glTranslatef(3, 1 - (x * 2), -(y - .5) * .75)
                glRotatef(-90, 0, 0, 1)

        elif side in [Module.Top, Module.Bottom]:
            if side == Module.Top:
                glTranslatef(-2 + (x * 2), 2, (y - .5) * .75)
            else:
                glTranslatef(-2 + (x * 2), -2, (y - .5) * .75)
                glRotatef(180, 1, 0, 0)

        elif side in [Module.Front, Module.Rear]:
            if side == Module.Front:
                glTranslatef((x - 1) * 1.95, (-(y - .5) * 1.90), 1)
                glRotatef(90, 1, 0, 0)
            else:
                glTranslatef(((x - 1) * 1.95), (-(y - .5) * 1.90), -1)
                glRotatef(90, 1, 0, 0)
                glRotatef(180, 0, 0, 1)

    # noinspection PyUnusedLocal
    def _update_status(self, dt, **kwargs):
        """Updates the status LED on disarmable modules"""
        # noinspection PyUnresolvedReferences
        if self._model.disarmed:
            set_material_color(self._status.materials['LED'], *Module.status_led[True][0])
            clock.unschedule(self._update_status)
            return

        self._elapsed += dt
        if self._elapsed <= 0.2:
            set_material_color(self._status.materials['LED'], *Module.status_led[False][1])
        elif self._elapsed < 1:
            set_material_color(self._status.materials['LED'], *Module.status_led[False][0])
        elif self._elapsed >= 1:
            self._elapsed = 0

