import pytest
from mock import mock

from bomb_defusal.modules import Passwords


class TestPasswords(object):
    def test_invalid_password(self):
        with pytest.raises(ValueError):
            Passwords(None, False, 'qrewt')

    def test_settings(self):
        actual_password = Passwords(None, False, 'after').setting
        assert isinstance(actual_password, str)
        assert len(actual_password) == 5

    def test_rotate_disarm_skip(self):
        subject = Passwords(None, True, 'after')
        initial = subject.setting
        subject.rotate_up(1)
        subject.rotate_down(3)
        assert initial == subject.setting

    def test_rotate_up(self):
        subject = Passwords(None, False, 'after')
        initial = subject.setting
        for i in range(4):
            [subject.rotate_up(p) for p in range(5)]
            assert initial != subject.setting
        [subject.rotate_up(p) for p in range(5)]
        assert initial == subject.setting

    def test_rotate_down(self):
        subject = Passwords(None, False, 'after')
        initial = subject.setting
        for i in range(4):
            [subject.rotate_down(p) for p in range(5)]
            assert initial != subject.setting
        [subject.rotate_down(p) for p in range(5)]
        assert initial == subject.setting

    def test_invalid_try(self):
        subject = Passwords(None, False, 'never')
        subject.on_wrong_try = mock.MagicMock()
        if subject.setting == 'never':
            subject.rotate_down(0)
        subject.submit()
        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

    def test_correct_order(self):
        password = 'think'
        subject = Passwords(None, False, password)
        subject.on_wrong_try = mock.MagicMock()
        for p in range(0, 5):
            for _ in range(0, 5):
                if subject.setting[p] == password[p]:
                    break
                subject.rotate_down(p)

        subject.submit()
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()