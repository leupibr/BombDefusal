import pytest
from mock import mock

from bomb_defusal.modules.simon_says import SimonSays


class TestSimonSays(object):
    @staticmethod
    def _assert_disarm_success(subject, press_order):
        subject.on_wrong_try = mock.MagicMock()

        for b in press_order[:1] + press_order[:2] + press_order[:3] + press_order[:]:
            assert not subject.disarmed
            subject.press(b)

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, press_order):
        subject.on_wrong_try = mock.MagicMock()
        for b in press_order:
            assert not subject.disarmed
            subject.press(b)

        assert not subject.disarmed
        subject.on_wrong_try.assert_called()

    @pytest.mark.parametrize('strikes,flash_order,press_order', [
        (0, ['Red', 'Green', 'Blue', 'Yellow'], ['Blue', 'Yellow', 'Red', 'Green']),
        (1, ['Red', 'Green', 'Blue', 'Yellow'], ['Yellow', 'Blue', 'Green', 'Red']),
        (2, ['Red', 'Green', 'Blue', 'Yellow'], ['Green', 'Yellow', 'Red', 'Blue']),
    ])
    def test_correct_order_and_vowel(self, strikes, flash_order, press_order):
        bomb = mock.MagicMock()
        bomb.serial_number.has_vowel = True
        bomb.timer.strikes = strikes

        subject = SimonSays(None, False, flash_order, bomb)
        self._assert_disarm_success(subject, press_order)

    @pytest.mark.parametrize('strikes,flash_order,press_order', [
        (0, ['Red', 'Green', 'Blue', 'Yellow'], ['Blue', 'Green', 'Yellow', 'Red']),
        (1, ['Red', 'Green', 'Blue', 'Yellow'], ['Red', 'Yellow', 'Blue', 'Green']),
        (2, ['Red', 'Green', 'Blue', 'Yellow'], ['Yellow', 'Blue', 'Green', 'Red']),
    ])
    def test_correct_order_and_no_vowel(self, strikes, flash_order, press_order):
        bomb = mock.MagicMock()
        bomb.serial_number.has_vowel = False
        bomb.timer.strikes = strikes

        subject = SimonSays(None, False, flash_order, bomb)
        self._assert_disarm_success(subject, press_order)

    @pytest.mark.parametrize('strikes,flash_order,press_order', [
        (0, ['Red', 'Green', 'Blue', 'Yellow'], ['Blue', 'Green', 'Yellow', 'Red']),
        (1, ['Red', 'Green', 'Blue', 'Yellow'], ['Red', 'Yellow', 'Blue', 'Green']),
        (2, ['Red', 'Green', 'Blue', 'Yellow'], ['Yellow', 'Blue', 'Green', 'Red']),
    ])
    def test_invalid_order_and_vowel(self, strikes, flash_order, press_order):
        bomb = mock.MagicMock()
        bomb.serial_number.has_vowel = True
        bomb.timer.strikes = strikes

        subject = SimonSays(None, False, flash_order, bomb)
        self._assert_disarm_failure(subject, press_order)

    @pytest.mark.parametrize('strikes,flash_order,press_order', [
        (0, ['Red', 'Green', 'Blue', 'Yellow'], ['Blue', 'Yellow', 'Red', 'Green']),
        (1, ['Red', 'Green', 'Blue', 'Yellow'], ['Yellow', 'Blue', 'Green', 'Red']),
        (2, ['Red', 'Green', 'Blue', 'Yellow'], ['Green', 'Yellow', 'Red', 'Blue']),
    ])
    def test_invalid_order_and_no_vowel(self, strikes, flash_order, press_order):
        bomb = mock.MagicMock()
        bomb.serial_number.has_vowel = False
        bomb.timer.strikes = strikes

        subject = SimonSays(None, False, flash_order, bomb)
        self._assert_disarm_failure(subject, press_order)

    @pytest.mark.parametrize('flash_order', [
        (['Red', 'Black', 'Blue', 'Yellow']),
        (['Red', 'Green']),
        (['Red', 'Green', 'Blue', 'Yellow', 'Yellow']),
    ])
    def test_invalid_flash_order(self, flash_order):
        with pytest.raises(ValueError):
            SimonSays(None, False, flash_order, None)

    @pytest.mark.parametrize('inputs,flash_order,flash_sequence', [
        (0, ['Red', 'Green', 'Blue', 'Yellow'], ['Red']),
        (1, ['Red', 'Green', 'Blue', 'Yellow'], ['Red', 'Green']),
        (2, ['Red', 'Green', 'Blue', 'Yellow'], ['Red', 'Green', 'Blue']),
        (3, ['Red', 'Green', 'Blue', 'Yellow'], ['Red', 'Green', 'Blue', 'Yellow']),
        (4, ['Red', 'Green', 'Blue', 'Yellow'], []),
    ])
    def test_flash_sequence(self, inputs, flash_order, flash_sequence):
        bomb = mock.MagicMock()
        bomb.serial_number.has_vowel = True
        bomb.timer.strikes = 0

        subject = SimonSays(None, False, flash_order, bomb)
        for i in range(inputs * (inputs + 1) // 2):
            subject.press(subject._press_order[0])
        assert subject.flash_order == flash_sequence
