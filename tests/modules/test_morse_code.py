import pytest
from mock import mock

from bomb_defusal.modules.morse_code import MorseCode


class TestMorseCode(object):
    @staticmethod
    def _assert_disarm_success(subject, frequency):
        subject.on_wrong_try = mock.MagicMock()

        TestMorseCode._find_frequency(subject, frequency)
        subject.transmit()
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, frequency):
        subject.on_wrong_try = mock.MagicMock()

        TestMorseCode._find_frequency(subject, frequency)
        subject.transmit()
        assert not subject.disarmed
        subject.on_wrong_try.assert_called()

    @staticmethod
    def _find_frequency(subject, frequency):
        for _ in range(len(subject.frequencies)):
            if subject.frequency == frequency:
                break
            subject.previous()
        else:
            for _ in range(len(subject.frequencies)):
                if subject.frequency == frequency:
                    break
                subject.next()

    def test_word(self):
        assert 'brick' == MorseCode(None, False, 'brick').word

    def test_code(self):
        subject = MorseCode(None, False, 'strobe')
        assert ['...', '−', '.−.', '−−−', '−...', '.'] == subject.code

    @pytest.mark.parametrize('word,frequency', [
        ('slick', '3.522 MHz'),
        ('leaks', '3.542 MHz'),
        ('flick', '3.555 MHz')
    ])
    def test_correct_frequency(self, word, frequency):
        subject = MorseCode(None, False, word)
        self._assert_disarm_success(subject, frequency)

    @pytest.mark.parametrize('word,frequency', [
        ('break', '3.592 MHz'),
        ('sting', '3.600 MHz'),
        ('beats', '3.572 MHz')
    ])
    def test_wrong_frequency(self, word, frequency):
        subject = MorseCode(None, False, word)
        self._assert_disarm_failure(subject, frequency)

    def test_invalid_word(self):
        with pytest.raises(ValueError):
            MorseCode(None, False, 'Invalid')

    def test_index(self):
        subject = MorseCode(None, False, 'break')
        start_index = subject.index
        subject.next()
        assert start_index + 1 == subject.index
        subject.previous()
        subject.previous()
        assert start_index - 1 == subject.index

    def test_disarmed_no_action(self):
        subject = MorseCode(None, True, 'break')
        start_index = subject.index
        subject.next()
        assert start_index == subject.index
        subject.previous()
        assert start_index == subject.index
