
from datetime import timedelta

import pytest
from mock import mock

import bomb_defusal.modules.module
import bomb_defusal.modules.venting_gas
from bomb_defusal.modules.venting_gas import VentingGas
from tests.utils.controllable_clock import ControllableClock


@pytest.fixture(autouse=True)
def clock(monkeypatch):
    clock_ = ControllableClock()
    monkeypatch.setattr(bomb_defusal.modules.module, 'clock', clock_)
    monkeypatch.setattr(bomb_defusal.modules.venting_gas, 'clock', clock_)
    return clock_


class TestVentingGas(object):
    def test_missed_interaction(self, clock):
        subject = VentingGas(None, timedelta(seconds=2))
        subject.on_wrong_try = mock.MagicMock()

        subject.activate()
        clock.fire_interval_schedule(0)
        assert subject.expiration == timedelta(seconds=1)
        clock.fire_interval_schedule(0)

        assert subject.expiration == timedelta(seconds=2)
        subject.on_wrong_try.assert_called_once()

    def test_no(self, clock):
        subject = VentingGas(None, timedelta(seconds=2))

        assert subject.text == 'VENTING\nCOMPLETE'
        subject.activate()
        assert subject.text == 'VENT GAS?\nY/N'
        subject.no()
        assert subject.text == 'VENT GAS?\nY/N\nNO'
        clock.fire_once_schedule(0)
        assert subject.text == 'VENTING\nPREVENTS\nEXPLOSIONS'
        clock.fire_once_schedule(0)
        assert subject.text == 'VENT GAS?\nY/N'
        clock.fire_interval_schedule(0)

    def test_double_no(self, clock):
        subject = VentingGas(None, timedelta(seconds=2))
        subject.no()
        assert 2 == len(clock.once_schedules)
        subject.no()
        assert 2 == len(clock.once_schedules)

    def test_yes(self, clock):
        subject = VentingGas(None, timedelta(seconds=2))
        assert subject.text == 'VENTING\nCOMPLETE'
        subject.activate()
        clock.fire_interval_schedule(0)
        assert subject.text == 'VENT GAS?\nY/N'
        subject.yes()
        assert subject.text == 'VENT GAS?\nY/N\nYES'
        clock.fire_once_schedule(0)
        assert subject.text == 'VENT GAS?\nY/N'
        assert subject.expiration == timedelta(seconds=2)

    def test_double_yes(self, clock):
        subject = VentingGas(None, timedelta(seconds=2))
        subject.yes()
        assert 1 == len(clock.once_schedules)
        subject.yes()
        assert 1 == len(clock.once_schedules)

