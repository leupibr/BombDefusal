from bomb_defusal.modules.module import Module


class Charge(Module):
    """This module is a placeholder for empty slots."""
    def __init__(self, position):
        """
        Initializes a new instance of the :class:`Charge` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        """
        super().__init__(position)
