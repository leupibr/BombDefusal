from enum import Enum, unique

from bomb_defusal.modules.module import Module


@unique
class PortType(Enum):
    DVI_D = 'DVI-D'
    PARALLEL = 'Parallel'
    PS_2 = 'PS/2'
    RJ_45 = 'RJ-45'
    SERIAL = 'Serial'
    RCA = 'Stereo RCA'


class Port(Module):
    """This module represent ports placed on the bomb."""
    def __init__(self, position, port):
        """
        Initializes a new instance of the :class:`Port` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param PortType port: Port that the module represents.
        :raises ValueError: If one of the attributes is invalid.
        """
        super().__init__(position)

        if not isinstance(port, PortType):
            raise ValueError('Invalid port')

        self._port = port

    @property
    def port(self):
        """
        Label of the indicator

        :rtype: PortType
        """
        return self._port
