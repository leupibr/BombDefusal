import pytest

from bomb_defusal.modules import SerialNumber


class TestSerialNumber(object):
    @pytest.mark.parametrize('number', ['123456', '1a2b3c', 'A1B2C3'])
    def test_valid_number(self, number):
        assert number == SerialNumber(None, number).serial_number
        assert number == str(SerialNumber(None, number))

    @pytest.mark.parametrize('number', ['abcdef', '', 'ABCDEF'])
    def test_invalid_number(self, number):
        with pytest.raises(ValueError):
            number = SerialNumber(None, number).serial_number

        with pytest.raises(ValueError):
            str(SerialNumber(None, number))

    @pytest.mark.parametrize('number', ['fde1', 'ser3', 'ser5', 'ser7t', 'ser9x'])
    def test_is_odd(self, number):
        assert SerialNumber(None, number).is_odd
        assert not SerialNumber(None, number).is_even

    @pytest.mark.parametrize('number', ['fd0e', 'ser2', 'ser4', 'ser6t', 'ser8x'])
    def test_is_even(self, number):
        assert not SerialNumber(None, number).is_odd
        assert SerialNumber(None, number).is_even

    @pytest.mark.parametrize('number', ['de2cc', 'o123', 'U3U', 'i13', 'ds1fa'])
    def test_has_vowel(self, number):
        assert SerialNumber(None, number).has_vowel

    @pytest.mark.parametrize('number', ['dx2cc', 'f123', 'Z3Z', 'k13', 'dsf5'])
    def test_has_no_vowel(self, number):
        assert not SerialNumber(None, number).has_vowel
