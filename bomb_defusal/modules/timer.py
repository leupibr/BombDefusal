import datetime
from time import sleep

from bomb_defusal.modules.module import Module


class CountdownTimer(Module):
    """Module, that counts down from a given duration to zero. It calls the event ``on_time_up`` if zero is reached."""
    def __init__(self, position, duration):
        """
        Initializes a new instance of the :class:`CountdownTimer` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param int duration: ...in seconds until the timer is at the end. Timer needs to be started with the
            :meth:`run` method.
        """
        super().__init__(position)

        self._time_left = duration
        self._strikes = 0
        self._running = False

        self.on_time_up = lambda: None
        self.on_strike = lambda: None
        self.on_strikes_reached = lambda: None

    @property
    def strikes(self):
        """
        Number of strikes

        :rtype: int
        """
        return self._strikes

    @property
    def time_left(self):
        """
        Time left until zero.

        :rtype: datetime.timedelta
        """

        return max(datetime.timedelta(seconds=0), datetime.timedelta(seconds=self._time_left))

    def run(self, stop_event):
        """Starts the timer."""
        self._running = True
        while not stop_event.is_set():
            self._on_step()
            sleep(self._timeout())

        self._running = False

    def strike(self):
        """Increases the number of strikes"""
        self._strikes += 1
        if self._strikes >= 3:
            self.on_strikes_reached()
        else:
            self.on_strike()

    def _on_step(self):
        """Step that is executed on each timer step."""
        self._time_left -= 1
        if self._time_left <= 0:
            self.on_time_up()

    def _timeout(self):
        """
        Calculates an returns the timeout based on the strikes.

        :rtype: int
        """
        return {3: 0, 2: .25, 1: .5}.get(self.strikes, 1)
