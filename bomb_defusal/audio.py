from os import path

import pyglet
from pyglet.media import Player, SourceGroup


class PlayerSource(object):
    def __init__(self, source_file):
        """
        Initializes a new instance of the :type:`PlayerSource` class.

        :param str source_file: Path of the audio source to be played
        """
        self._player = Player()
        self._player.push_handlers(self.on_player_eos)
        self._is_playing = False
        self._source = pyglet.resource.media(source_file, streaming=False)

    @property
    def is_playing(self):
        """ Gets a value indication whether the source is playing or not """
        return self._is_playing

    def play(self, **kwargs):
        """ Starts the playback of the audio source if it's not already playing """
        if self.is_playing:
            return

        group = SourceGroup(self._source.audio_format, None)
        group.queue(self._source)
        group.loop = False

        self._player.volume = kwargs.get('volume', 1)
        self._player.queue(group)
        self._player.play()

    def stop(self):
        """ Stops the playback """
        self._player.next_source()
        self._is_playing = False

    def on_player_eos(self, *args, **kwargs):
        """ Callback handler of the player when the source has reached the end """
        self._is_playing = False


class Audio(object):
    def __init__(self, *sources, **kwargs):
        """ Initializes the audio management for the game"""
        assert sources

        pyglet.resource.path.append(kwargs.get('path', '../resources/audio'))
        pyglet.resource.reindex()

        self._disabled = kwargs.get('disabled', False)
        self._audio_sources = self._load_audio_sources(*sources)  # type: dict[str, PlayerSource]

    def play(self, name, **kwargs):
        """
        Plays the audio source with the given name

        :param str name: Name of the audio source to play
        """
        if self._disabled:
            return
        self._audio_sources[name].play(**kwargs)

    def stop_all(self):
        """ Stops the playback of all currently playing audio sources """
        if self._disabled:
            return
        for audio in self._audio_sources.values():
            audio.stop()

    def _load_audio_sources(self, *sources):
        """ Loads the audio resources for later use """
        return {
            path.splitext(source)[0]: PlayerSource(source)
            for source in sources
        }


_audio = None


def play(name, **kwargs):
    """
    Plays the audio source with the given name

    :param str name: Name of the audio source to play
    """
    global _audio
    return _audio.play(name, **kwargs)


def stop_all():
    """
    Stops all playing audio sources
    """
    global _audio
    return _audio.stop_all()


def init(*sources, **kwargs):
    """
    Initializes the audio manager

    :param sources: Audio sources to load
    """
    global _audio
    _audio = Audio(*sources, **kwargs)

