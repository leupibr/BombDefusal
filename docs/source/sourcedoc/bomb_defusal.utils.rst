bomb_defusal.utils
==================

bomb_defusal.utils.callable_collection
--------------------------------------

.. automodule:: bomb_defusal.utils.callable_collection
    :members:
    :undoc-members:
    :special-members: __call__, __iadd__, __isub__
    :show-inheritance:

