from bomb_defusal.view.components import Lcd, Led, Button
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import colors


class Memory(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Memory` class.

        :param bomb_defusal.modules.Memory model: Model of the memory module
        """
        super().__init__(model)

        self._display = Lcd((-.20, 0, -.2), static='resources/components/radar.obj',
                            foreground=(255, 255, 255), text_height=.3, text_offset=-.03)
        self._leds = [Led((.5, 0, z), color=colors['Red']) for z in [.55, .4, .25, .1, -.05]]
        self._buttons = [Button((-.5 + i * .25, 0, .45), static='resources/components/memory_button.obj',
                                text_height=.2, hit_on_box=True) for i in range(4)]

        self._update_buttons()
        self._update_stage()

    def on_draw(self):
        """Draw the 3D view of the module"""
        [led.draw() for led in self._leds]
        [button.draw() for button in self._buttons]
        self._display.draw()

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        for index, button in enumerate(self._buttons):
            distance = button.hit_test(*position)
            if distance:
                return self, index, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        self._buttons[tag].press()
        self._model.press(tag)

    def hit_release(self, tag, button, modifiers):
        self._buttons[tag].release()
        self._update_buttons()
        self._update_stage()

    def _update_buttons(self):
        self._display.text = self._model.label
        for i, label in enumerate(self._model.buttons):
            self._buttons[i].text = label

    def _update_stage(self):
        on = (1, 0, 0, 1) if self._model.disarm_stage < 5 else (0, .75, 0, 1)
        off = (.2, 0, 0, 1)

        for i in range(5):
            self._leds[i].color = on if i < self._model.disarm_stage else off
            self._leds[i].active = i < self._model.disarm_stage
