import datetime

import pytest
from mock import mock

from bomb_defusal.modules import Button


class TestButton(object):
    @staticmethod
    def _assert_disarm_success(subject):
        subject.on_wrong_try = mock.MagicMock()
        subject.open_lid()
        subject.hold()
        subject.release()

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject):
        subject.on_wrong_try = mock.MagicMock()
        subject.open_lid()
        subject.hold()
        subject.release()

        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

    strip_parametrization = pytest.mark.parametrize('strip,minutes,seconds', [
        ('Blue', 0, 4), ('Blue', 0, 46), ('Blue', 4, 31),
        ('Yellow', 0, 5), ('Yellow', 0, 56), ('Yellow', 5, 10),
        ('White', 0, 1), ('White', 0, 10), ('White', 1, 45),
        ('Green', 0, 1), ('Green', 0, 10), ('Green', 1, 30),
        ('Red', 0, 1), ('Red', 0, 10), ('Red', 1, 2),
    ])

    @strip_parametrization
    def test_blue_abort(self, strip, minutes, seconds):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=minutes, seconds=seconds)
        subject = Button(None, False, 'Abort', 'Blue', strip, bomb)
        subject.on_wrong_try = mock.MagicMock()
        self._assert_disarm_success(subject)
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    def test_multiple_batteries_detonate(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 2
        subject = Button(None, False, 'Detonate', 'Blue', None, bomb)
        subject.on_wrong_try = mock.MagicMock()
        self._assert_disarm_success(subject)
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @strip_parametrization
    def test_white_car_indicator(self, strip, minutes, seconds):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=minutes, seconds=seconds)
        bomb.indicators = [type('', (object,), dict(label='CAR'))]
        subject = Button(None, False, None, 'White', strip, bomb)
        self._assert_disarm_success(subject)

    def test_multiple_batteries_frk_indicator(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 3
        bomb.indicators = [type('', (object,), dict(label='FRK'))]
        subject = Button(None, False, None, 'White', None, bomb)
        self._assert_disarm_success(subject)

    @strip_parametrization
    def test_yellow(self, strip, minutes, seconds):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=minutes, seconds=seconds)
        subject = Button(None, False, None, 'Yellow', strip, bomb)
        self._assert_disarm_success(subject)

    def test_red(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        subject = Button(None, False, 'Hold', 'Red', None, bomb)
        self._assert_disarm_success(subject)

    @strip_parametrization
    def test_non_apply(self, strip, minutes, seconds):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=minutes, seconds=seconds)
        subject = Button(None, False, None, None, strip, bomb)
        self._assert_disarm_success(subject)

    def test_invalid_step(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=3, seconds=10)
        subject = Button(None, False, 'Detonate', 'Blue', 'Blue', bomb)
        self._assert_disarm_failure(subject)

    def test_open_close_lid(self):
        subject = Button(None, False, 'Detonate', 'Blue', 'Blue', None)
        subject.open_lid()
        assert subject.is_lid_open
        subject.close_lid()
        assert not subject.is_lid_open

    def test_hold_without_open_lid(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=4, seconds=4)
        subject = Button(None, False, None, None, 'Blue', bomb)

        subject.on_wrong_try = mock.MagicMock()
        with pytest.raises(AssertionError):
            subject.hold()
        subject.open_lid()
        subject.hold()

    def test_release_without_hold(self):
        bomb = mock.MagicMock()
        bomb.battery_count = 0
        bomb.timer.time_left = datetime.timedelta(minutes=4, seconds=4)
        subject = Button(None, False, None, None, 'Blue', bomb)

        subject.on_wrong_try = mock.MagicMock()

        subject.open_lid()
        with pytest.raises(AssertionError):
            subject.release()
        subject.hold()
        subject.release()
