from bomb_defusal.view.components import Text, Led
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import colors


class Indicator(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Indicator` class.

        :param bomb_defusal.modules.Indicator model: Model of the indicator module
        """
        super().__init__(model, base='resources/components/indicator.obj')
        self._led = Led((-.32, .1, 0), color=(colors['Green']), rotate_y=90)
        self._led.active = True

        self._text = Text((-0.15, 0, 0), text_height=.1, foreground=(255, 255, 255), rotate_y=90)
        self._text.text = model.label.value

    def on_draw(self):
        self._led.draw()
        self._text.draw()
