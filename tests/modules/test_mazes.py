import mock
import pytest

from bomb_defusal.modules import Mazes, MazesShape


class TestMazes(object):
    @pytest.mark.parametrize('start', [True, False])
    @pytest.mark.parametrize('pos', [
        (-1, 1), (5, -8), (None, 3), (3, None), (4, 6), (16, 2)
    ])
    def test_invalid_pos(self, pos, start):
        start, end = (pos, (0, 0)) if start else ((0, 0), pos)
        with pytest.raises(ValueError):
            Mazes(None, False, MazesShape.One, start, end)

    def test_invalid_shape(self):
        with pytest.raises(ValueError):
            Mazes(None, False, None, (0, 0), (5,5))

    def test_marker(self):
        subject = Mazes(None, False, MazesShape.One, (0, 0), (5, 5))
        assert ((0, 1), (5, 2)) == subject.marker

    def test_end_pos(self):
        subject = Mazes(None, False, MazesShape.One, (0, 0), (5, 5))
        assert (5, 5) == subject.target

    def test_cursor(self):
        subject = Mazes(None, False, MazesShape.One, (0, 0), (5, 5))
        assert (0, 0) == subject.cursor
        subject.move('R')
        assert (1, 0) == subject.cursor
        subject.move('R')
        assert (2, 0) == subject.cursor
        subject.move('D')
        assert (2, 1) == subject.cursor

    def test_disarm_shape_one(self):
        subject = Mazes(None, False, MazesShape.One, (0, 0), (5, 5))
        subject.on_wrong_try = mock.MagicMock()
        order = 'RRDLDRDRURRDDD'
        for d in order:
            subject.move(d)

        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    def test_disarmed_move(self):
        subject = Mazes(None, True, MazesShape.One, (0, 0), (5, 5))
        initial_pos = subject.position
        subject.move('R')
        assert initial_pos == subject.position

    def test_invalid_try(self):
        subject = Mazes(None, False, MazesShape.One, (0, 0), (5, 5))
        subject.on_wrong_try = mock.MagicMock()
        order = 'RRR'
        for d in order:
            subject.on_wrong_try.assert_not_called()
            assert not subject.disarmed
            subject.move(d)

        assert not subject.disarmed
        subject.on_wrong_try.assert_called_once()

