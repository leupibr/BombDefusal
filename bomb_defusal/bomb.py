import bomb_defusal.modules
from bomb_defusal.modules.timer import CountdownTimer


class Bomb(object):
    def __init__(self):
        self._modules = []
        self.on_detonate = lambda: None

    @property
    def modules(self):
        return self._modules

    @property
    def serial_number(self):
        return list(filter(lambda m: type(m) == bomb_defusal.modules.SerialNumber, self._modules))[0]

    @property
    def battery_count(self):
        return sum([b.units for b in filter(lambda m: type(m) == bomb_defusal.modules.Battery, self._modules)], 0)

    @property
    def indicators(self):
        return filter(lambda m: type(m) == bomb_defusal.modules.Indicator, self._modules)

    @property
    def timer(self):
        return list(filter(lambda m: type(m) == bomb_defusal.modules.CountdownTimer, self._modules))[0]

    @property
    def ports(self):
        return [m.port for m in self._modules if isinstance(m, bomb_defusal.modules.Port)]

    def add_module(self, *modules):
        for m in modules:
            if m.position in [m.position for m in self._modules]:
                raise AttributeError('Slot %r already used!' % str(m.position))
            if isinstance(m, CountdownTimer):
                m.on_time_up = lambda *a, **kw: self.on_detonate()
                m.on_strikes_reached = lambda *a, **kw: self.on_detonate()
            self._modules.append(m)
