import datetime

import mock
import pytest

from bomb_defusal.modules import CountdownTimer


class TestCountdownTimer(object):

    def test_strikes(self):
        subject = CountdownTimer(('Front', 1), 600)
        assert subject.strikes == 0

        subject.strike()
        assert subject.strikes == 1

        subject.strike()
        assert subject.strikes == 2

    def test_time_left(self):
        subject = CountdownTimer(('Front', 1), 600)
        assert datetime.timedelta(minutes=10) == subject.time_left

        subject._on_step()
        assert datetime.timedelta(minutes=9, seconds=59) == subject.time_left

    def test_on_time_up(self):
        subject = CountdownTimer(('Front', 1), 2)
        subject.on_time_up = mock.MagicMock()

        subject._on_step()
        subject.on_time_up.assert_not_called()

        subject._on_step()
        subject.on_time_up.assert_called_once()

    def test_strikes_reached(self):
        subject = CountdownTimer(('Front', 1), 600)
        subject.on_strikes_reached = mock.MagicMock()
        for i in range(3):
            subject.on_strikes_reached.assert_not_called()
            subject.strike()
        subject.on_strikes_reached.assert_called_once()

    @pytest.mark.parametrize('strikes,timeout', [(0, 1), (1, .5), (2, .25), (3, 0)])
    def test_timeout(self, strikes, timeout):
        subject = CountdownTimer(('Front', 1), 600)
        for s in range(strikes):
            subject.strike()
        assert timeout == subject._timeout()

