import math

from PIL import ImageFont, Image, ImageDraw
from pyglet.gl import *
from pyglet.image import ImageData, Texture
from pywavefront import Wavefront
from pywavefront.mesh import Mesh

from bomb_defusal.view.utils.helper import grouper

Rectangle = [(0, 0), (0, 1), (1, 1), (1, 0)]


def draw_texture(texture, **kwargs):
    """
    Draws a rectangle and maps the given texture on it

    :param Texture texture: Texture to draw on the rectangle at the given position
    """
    dimension = kwargs.get('dimension')
    position = kwargs.get('position', (0, 0, 0))
    keep_aspect = kwargs.get('keep_aspect', False)
    align = kwargs.get('align', 'middle')

    glEnable(texture.target)
    glBindTexture(texture.target, texture.id)

    if keep_aspect:
        height = dimension[1]
        width = height * texture.width / texture.height
    else:
        height = dimension[1]
        width = dimension[0]

    if align == 'left':
        x_offset = 0
    elif align == 'right':
        x_offset = -width
    else:
        x_offset = -width / 2

    uv = Rectangle[:]
    vertices = [(v[0] * width, 0, v[1] * height) for v in Rectangle]

    glBegin(GL_QUADS)
    for i in range(4):
        glTexCoord2f(*uv[i])
        glVertex3f(
            position[0] + x_offset + vertices[i][0],
            position[1] + vertices[i][1],
            position[2] + -(height / 2) + vertices[i][2])
    glEnd()

    glDisable(texture.target)


def create_font_texture(text, background, foreground, **kwargs):
    """
    Renders the given text into a font-texture

    :param str text: Text to render
    :param tuple[int] background: Background color of the texture
    :param tuple[int] foreground: Color of the text
    :return: pyglet.image.Texture Texture containing the given text
    """
    font_file = kwargs.get('font', 'resources/fonts/sans.ttf')
    font_size = kwargs.get('font_size', 64)
    font = ImageFont.truetype(font=font_file, size=font_size)

    text_size = font.getsize(text)
    text_size = [text_size[0], text_size[1]]
    size = [1 << (x - 1).bit_length() for x in text_size]

    image = Image.new('RGBA', size, background)
    draw = ImageDraw.Draw(image)
    draw.text(((size[0] - text_size[0]) / 2, (size[1] - text_size[1]) / 2), text, foreground, font=font)
    data = ImageData(*size, 'RGBA', image.tobytes(), pitch=size[0] * 4)

    texture = Texture.create(width=size[0], height=size[1])
    texture.blit_into(data, x=0, y=0, z=0)
    return texture


def set_material_color(material, ambient, diffuse=None, specular=None, emissive=None):
    """
    Sets the material color to the given values

    :param Union[Wavefront, Mesh, Material] material: Material to set the value
    :param iterable[float] ambient: Ambient color of the material
    :param iterable[float] diffuse: Diffuse color of the material
    :param iterable[float] specular: Specular color of the material
    :param iterable[float] emissive: Emissive color of the material
    """
    if isinstance(material, Wavefront):
        for mat in material.materials.values():
            set_material_color(mat, ambient, diffuse, specular, emissive)
        return

    if isinstance(material, Mesh):
        for mat in material.materials:
            set_material_color(mat, ambient, diffuse, specular, emissive)
        return

    if diffuse is None and specular is None:
        specular = specular or ambient
        diffuse = diffuse or [ambient[0] * .6, ambient[1] * .6, ambient[2] * .6, ambient[3]]
        ambient = [ambient[0] * .4, ambient[1] * .4, ambient[2] * .4, ambient[3]]

    material.ambient = ambient
    material.diffuse = diffuse
    material.specular = specular
    material.emissive = emissive or (0, 0, 0, 0)


def translate(vertices, dx, dy, dz):
    """
    Moves the origin of the vertices by the given offset

    :param Union[Sequence[Tuple[int, int, int]], Wavefront, Mesh] vertices: Mesh to move the 3D origin
    :param float dx: X-Axis difference
    :param float dy: Y-Axis difference
    :param float dz: Z-Axis difference
    """
    if isinstance(vertices, Wavefront):
        for m in vertices.meshes.values():
            translate(m, dx, dy, dz)
        return

    if isinstance(vertices, Mesh):
        for material in vertices.materials:
            translate(material.vertices, dx, dy, dz)
        return

    original = list(grouper(vertices, 8))
    vertices.clear()
    for vertex in original:
        vertices.extend([
            vertex[0], vertex[1],
            vertex[2], vertex[3], vertex[4],
            vertex[5] + dx, vertex[6] + dy, vertex[7] + dz])


def scale(vertices, dx, dy, dz):
    """
    Scale the mesh by the given factors

    :param Union[Sequence[Tuple[int, int, int]], Wavefront, Mesh] vertices: Mesh to move scale
    :param float dx: X-Axis factor
    :param float dy: Y-Axis factor
    :param float dz: Z-Axis factor
    """
    if isinstance(vertices, Wavefront):
        for m in vertices.meshes.values():
            scale(m, dx, dy, dz)
        return

    if isinstance(vertices, Mesh):
        for material in vertices.materials:
            scale(material.vertices, dx, dy, dz)
        return

    original = list(grouper(vertices, 8))
    vertices.clear()
    for vertex in original:
        vertices.extend([
            vertex[0], vertex[1],
            vertex[2], vertex[3], vertex[4],
            vertex[5] * dx, vertex[6] * dy, vertex[7] * dz])


def rotate_y(vertices, angle):
    """
    Rotates the vertices around the Y axis. This is a slow operation. Avoid in the render loop!:

    :param Union[Sequence[Tuple[int, int, int]], Wavefront, Mesh] vertices: Mesh to rotate
    :param float angle: Y-Axis rotation angle in degrees
    """
    if isinstance(vertices, Wavefront):
        for m in vertices.meshes.values():
            rotate_y(m, angle)
        return

    if isinstance(vertices, Mesh):
        for material in vertices.materials:
            rotate_y(material.vertices, angle)
        return

    original = list(grouper(vertices, 8))
    radians = angle * math.pi / 180
    cos_a = math.cos(radians)
    sin_a = math.sin(radians)

    vertices.clear()
    for vertex in original:
        vertices.extend([
            vertex[0], vertex[1],
            vertex[4] * sin_a + vertex[2] * cos_a, vertex[3], vertex[4] * cos_a - vertex[2] * sin_a,
            vertex[7] * sin_a + vertex[5] * cos_a, vertex[6], vertex[7] * cos_a - vertex[5] * sin_a])
