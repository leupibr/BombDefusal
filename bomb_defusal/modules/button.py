from bomb_defusal.modules.module import DisarmableModule


class Button(DisarmableModule):
    """The Button module is a button, that needs to be pressed and released the correct way."""
    def __init__(self, position, disarmed, subject, button_color, strip_color, bomb):
        """
        Initializes a new instance of the :class:`Button` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Tuple that indicates whether the module is disarmed or not.
        :param str subject: Buttons text.
        :param str button_color: Color of the button.
        :param str strip_color: Color of the strip.
        :param bomb_defusal.bomb.Bomb bomb: Reference to the whole bomb to get timer, indicators and battery information.
        """
        super().__init__(position, disarmed)

        self._is_lid_open = False
        self._subject = subject
        self._button_color = button_color
        self._strip = [strip_color, False]
        self._bomb = bomb
        self._held = False

    @property
    def subject(self):
        """
        Gets the label on the button.

        :rtype: str
        """
        return self._subject

    @property
    def button_color(self):
        """
        Gets the color of the button

        :rtype: str
        """
        return self._button_color

    @property
    def strip_color(self):
        """
        Gets the color of the strip

        :rtype: str
        """
        if self._strip[1]:
            return self._strip[0]

    @property
    def is_lid_open(self):
        """
        Actual state of the lid

        :return: `True` if lid is open, `False` otherwise
        :rtype: bool
        """
        return self._is_lid_open

    def close_lid(self):
        """Closes the lid"""
        self._is_lid_open = False

    def hold(self):
        """Holds the button down."""
        assert self._is_lid_open, 'Lid is not open'

        if self.button_color == 'Blue' and self.subject == 'Abort':
            self._enable_strip()
        elif self._bomb.battery_count > 1 and self.subject == 'Detonate':
            pass
        elif self.button_color == 'White' and any(i.label == 'CAR' for i in self._bomb.indicators):
            self._enable_strip()
        elif self._bomb.battery_count > 2 and any(i.label == 'FRK' for i in self._bomb.indicators):
            pass
        elif self.button_color == 'Yellow':
            self._enable_strip()
        elif self.button_color == 'Red' and self.subject == 'Hold':
            pass
        else:
            self._enable_strip()
        self._held = True

    def open_lid(self):
        """Opens the lid"""
        self._is_lid_open = True

    def release(self):
        """Releases the button"""
        assert self._is_lid_open, 'Lid is not open'
        assert self._held, 'Button must be hold first'

        if not self.strip_color:
            self._disarmed = True
        elif self.strip_color == 'Blue':
            self._disarmed = '4' in str(self._bomb.timer.time_left)
        elif self.strip_color == 'Yellow':
            self._disarmed = '5' in str(self._bomb.timer.time_left)
        else:
            self._disarmed = '1' in str(self._bomb.timer.time_left)

        if not self._disarmed:
            self.on_wrong_try()

    def _enable_strip(self):
        """Enables the strip"""
        self._strip[1] = True

