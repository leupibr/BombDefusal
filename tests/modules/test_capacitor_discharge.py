from datetime import timedelta

import pytest
from mock import mock

import bomb_defusal.modules.capacitor_discharge
import bomb_defusal.modules.module
from bomb_defusal.modules import CapacitorDischarge
from tests.utils.controllable_clock import ControllableClock


@pytest.fixture(autouse=True)
def clock(monkeypatch):
    clock_ = ControllableClock()
    monkeypatch.setattr(bomb_defusal.modules.module, 'clock', clock_)
    monkeypatch.setattr(bomb_defusal.modules.capacitor_discharge, 'clock', clock_)
    return clock_


class TestCapacitorDischarge(object):
    def test_no_interaction(self, clock):
        subject = CapacitorDischarge(None, timedelta(seconds=1))
        subject.on_wrong_try = mock.MagicMock()
        subject.activate()
        clock.fire_interval_schedule(1)
        subject.on_wrong_try.assert_called_once()
        assert timedelta(seconds=1) == subject.expiration

    def test_hold_partial(self, clock):
        subject = CapacitorDischarge(None, timedelta(seconds=5))
        subject.on_wrong_try = mock.MagicMock()
        subject.activate()
        clock.fire_interval_schedule(1)
        clock.fire_interval_schedule(1)
        clock.fire_interval_schedule(1)
        subject.hold()
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)
        subject.release()
        clock.fire_interval_schedule(0)
        assert timedelta(seconds=4) == subject.expiration

    def test_hold_full(self, clock):
        subject = CapacitorDischarge(None, timedelta(seconds=3))
        subject.on_wrong_try = mock.MagicMock()
        subject.activate()
        clock.fire_interval_schedule(1)
        clock.fire_interval_schedule(1)
        subject.hold()
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)
        clock.fire_interval_schedule(0)
        subject.release()
        assert timedelta(seconds=3) == subject.expiration
