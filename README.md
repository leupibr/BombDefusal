[![Build Status](https://travis-ci.org/leupibr/BombDefusal.svg?branch=develop)](https://travis-ci.org/leupibr/BombDefusal)
[![Documentation Status](https://readthedocs.org/projects/bombdefusal/badge/?version=latest)](http://bombdefusal.readthedocs.io/en/latest/?badge=latest)
[![Maintainability](https://api.codeclimate.com/v1/badges/daf936eb0b9a0dd6b912/maintainability)](https://codeclimate.com/github/leupibr/BombDefusal/maintainability)
[![Coverage Status](https://coveralls.io/repos/github/leupibr/BombDefusal/badge.svg)](https://coveralls.io/github/leupibr/BombDefusal)
[![Github Issues](http://githubbadges.herokuapp.com/leupibr/BombDefusal/issues.svg)](https://github.com/leupibr/BombDefusal/issues)
[![Pending Pull-Requests](http://githubbadges.herokuapp.com/leupibr/BombDefusal/pulls.svg)](https://github.com/leupibr/BombDefusal/pulls)
[![License](http://img.shields.io/:license-mit-blue.svg)](http://badges.mit-license.org)

# Bomb Defusal
Bomb Defusal

## Contributors
* [Bruno Leupi](https://gihub.com/leupibr)
* [Markus Gilli](https://gihub.com/gillima)
