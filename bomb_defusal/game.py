#!/usr/bin/env python3
from argparse import ArgumentParser

import pyglet
from pyglet import clock
from pyglet.gl import *
from pyglet.window import mouse

from bomb_defusal import view, modules, audio
from bomb_defusal.bomb import Bomb
from bomb_defusal.modules import PortType, MazesShape
from bomb_defusal.modules.module import DisarmableModule
from bomb_defusal.view.utils.helper import vector, load_model

pyglet.options['debug_gl'] = False
pyglet.options['audio'] = ('openal', 'directsound', 'silent')


class Game(object):
    def __init__(self, bomb, **config):
        """
        Initializes a new instance of the :class:`Game` class.

        :param bomb_defusal.bomb.Bomb bomb: Model of the bomb to visualize
        :param dict config: View configuration of the bomb
        """
        self._background = config.get('background', [0.2, 0.2, 0.2, 1])
        self._ambient = config.get('ambient', [.3, .3, .3, 1])  # no light
        self._diffuse = config.get('diffuse', [.7, .7, .7, 1])  # light
        self._specular = config.get('specular', [.4, .4, .4, 1])  # reflect
        self._light_position = config.get('light_position', [-3, 5, 15, 1])

        self._room = load_model('resources/room.obj')
        self._bomb = view.Bomb(bomb)
        self._zoom = 35
        self._detonated = False

        bomb.timer.on_strike = self.on_strike
        bomb.on_detonate = self.on_detonate

        self._window = self._create_window()
        self._fps_display = pyglet.window.FPSDisplay(self._window) if config.get('show_fps', False) else None
        self._setup_hooks()
        self._setup_3d(self._window.width, self._window.height)

    def on_strike(self):
        audio.play('alert')

    def on_detonate(self):
        if self._detonated:
            return

        self._detonated = True
        audio.stop_all()
        audio.play('detonate')

    def on_draw(self):
        """ Draw handler for the pyglet window """
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(int(self._zoom), self._window.width / self._window.height, 1, 4096)
        gluLookAt(2.5, 3.2, 10, 0, 0, 0, 0, 1, 0)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        if not self._detonated:
            self._room.draw()
            self._bomb.draw()
        else:
            glClearColor(0, 0, 0, 1)

        if self._fps_display:
            self._fps_display.draw()

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        """ Mouse drag handler for the pyglet window """
        if buttons & mouse.LEFT:
            self._bomb.rotation = [self._bomb.rotation[0] - dy // 2,
                                   self._bomb.rotation[1] + dx // 2,
                                   self._bomb.rotation[2]]

    def on_mouse_scroll(self, x, y, sx, sy):
        """ Mouse-scroll handler for the pyglet window """
        self._zoom -= sy * 0.5

    def on_mouse_press(self, x, y, button, modifiers):
        """ Mouse-down handler for the pyglet window """
        if not self._detonated:
            self._bomb.hit((x, y), button, modifiers)

    def on_mouse_release(self, x, y, button, modifiers):
        """ Mouse-down handler for the pyglet window """
        if not self._detonated and self._bomb.hit_release((x, y), button, modifiers):
            audio.play('click')

    def on_resize(self, width, height):
        """ Resize handler of the pyglet window """
        self._setup_3d(width, height)

    def run(self):
        """ Application runner of the bomb defusal game """
        pyglet.app.run()

    def _setup_hooks(self):
        """ Register event handler to the pyglet event system """
        self._window.push_handlers(
            self.on_draw,
            self.on_resize,
            self.on_mouse_drag,
            self.on_mouse_scroll,
            self.on_mouse_press,
            self.on_mouse_release)

    def _setup_3d(self, width, height):
        """ Setup the OpenGL 3D context """
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glViewport(0, 0, width, height)

        glShadeModel(GL_SMOOTH)
        glDepthFunc(GL_LEQUAL)
        glCullFace(GL_BACK)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glClearColor(*self._background)

        glEnable(GL_BLEND)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        glEnable(GL_LIGHTING)
        glEnable(GL_POLYGON_SMOOTH)

        glEnable(GL_LIGHT0)
        glLightfv(GL_LIGHT0, GL_AMBIENT, vector(*self._ambient))
        glLightfv(GL_LIGHT0, GL_DIFFUSE, vector(*self._diffuse))
        glLightfv(GL_LIGHT0, GL_SPECULAR, vector(*self._specular))
        glLightfv(GL_LIGHT0, GL_POSITION, vector(*self._light_position))

    def _create_window(self):
        """ Create the pyglet window using the best available configuration """
        platform = pyglet.window.get_platform()
        display = platform.get_default_display()
        screen = display.get_default_screen()
        try:
            template = pyglet.gl.Config(depth_size=24, sample_buffers=1, samples=4)
            config = screen.get_best_config(template)
        except pyglet.window.NoSuchConfigException:
            config = screen.get_best_config()
        return pyglet.window.Window(width=1024, height=768, caption="", resizable=True, config=config)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-s', '--no-sound', action='store_true', default=False)
    args = parser.parse_args()

    audio.init('alert.wav', 'beep.wav', 'click.wav', 'detonate.wav', path='../resources/audio', disabled=args.no_sound)

    bomb = Bomb()
    bomb.add_module(modules.SerialNumber(('Right', 0), 'ax215y'))
    bomb.add_module(modules.Indicator(('Right', 1), modules.IndicatorText.BOB))

    bomb.add_module(modules.Port(('Top', 0), PortType.DVI_D))
    bomb.add_module(modules.Port(('Top', 1), PortType.SERIAL))
    bomb.add_module(modules.Port(('Top', 2), PortType.PARALLEL))
    bomb.add_module(modules.Port(('Top', 3), PortType.RCA))
    bomb.add_module(modules.Port(('Top', 4), PortType.RJ_45))
    bomb.add_module(modules.Port(('Top', 5), PortType.PS_2))

    timer = modules.CountdownTimer(('Front', 4), 60 * 10)
    bomb.add_module(timer)

    passwords = modules.Passwords(('Front', 2), False, 'again')
    bomb.add_module(passwords)

    button = modules.Button(('Front', 3), False, 'Alert', 'Green', 'Blue', bomb)
    bomb.add_module(button)

    keypad = modules.Keypad(('Front', 5), False,
                            [modules.keypad.KeypadChar.AELIG, modules.keypad.KeypadChar.PSI,
                             modules.keypad.KeypadChar.SHORTI, modules.keypad.KeypadChar.OMEGA])
    bomb.add_module(keypad)

    who_is_on_first = modules.WhoIsOnFirst(('Front', 0), False, 'READY FIRST NO BLANK'.split(' '))
    bomb.add_module(who_is_on_first)

    memory = modules.Memory(('Front', 1), False, ['1', '2', '3', '4', '1'])
    bomb.add_module(memory)

    wire = modules.Wire(('Rear', 1), False, ['Red', 'Blue', 'Green', 'Yellow', 'Black', 'White'], bomb)
    bomb.add_module(wire)

    morse = modules.MorseCode(('Rear', 2), False, 'flick')
    bomb.add_module(morse)

    maze = modules.Mazes(('Rear', 3), False, MazesShape.One, (0, 0), (5, 5))
    bomb.add_module(maze)

    wire_sequence = modules.WireSequence(('Rear', 4), False, [('Red', 'B'), None, ('Blue', 'A'),
                                                              None, ('Blue', 'A'), ('Black', 'C'),
                                                              ('Black', 'C'), None, ('Red', 'B')])
    bomb.add_module(wire_sequence)

    complicated_wire = modules.ComplicatedWire(('Rear', 5), False, [
        (('Blue',), False, True),
        (('Red',), False, True),
        (('Black', 'Red'), False, True),
        (('Red', 'Blue'), True, False),
        (('Red', 'Blue'), False, False),
        (('Red', 'Blue'), True, False)], bomb)
    bomb.add_module(complicated_wire)

    simon_says = modules.SimonSays(('Rear', 0), False, ['Red', 'Green', 'Blue', 'Yellow'], bomb)
    bomb.add_module(simon_says)

    # bomb.add_module(modules.Charge(('Rear', 0)))
    bomb.add_module(modules.Battery(('Bottom', 0), 1))
    bomb.add_module(modules.Battery(('Bottom', 1), 2))

    for model in bomb.modules:
        if isinstance(model, DisarmableModule):
            model.on_wrong_try += timer.strike

    # bomb_events = BombEvents(bomb)
    game = Game(bomb, show_fps=True)
    game.run()
