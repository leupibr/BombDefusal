import random
from enum import Enum

from bomb_defusal.modules.module import NeedyModule


class KnobOrientation(Enum):
    Right = 0
    Up = 1
    Left = 2
    Down = 3


class LedSet(Enum):
    UpOne = '001011111101'
    UpTwo = '101010011011'
    DownOne = '011001111101'
    DownTwo = '101010010001'
    LeftOne = '000010100111'
    LeftTwo = '000010000110'
    RightOne = '101111111010'
    RightTwo = '101100111010'


class Knobs(NeedyModule):
    def __init__(self, position, expiration):
        super().__init__(position, expiration)

        self._orientation = KnobOrientation.Up
        self._led_set = random.choice(list(LedSet))
        self._on_time_up = self._check_orientation

    @property
    def leds(self):
        return self._led_set.value

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, value):
        self._orientation = value

    def _reset(self):
        super()._reset()
        self._led_set = random.choice(list(LedSet))

    def _check_orientation(self):
        if self.orientation == KnobOrientation.Right and self._led_set in {LedSet.RightOne, LedSet.RightTwo}:
            return

        if self.orientation == KnobOrientation.Up and self._led_set in {LedSet.UpOne, LedSet.UpTwo}:
            return

        if self.orientation == KnobOrientation.Left and self._led_set in {LedSet.LeftOne, LedSet.LeftTwo}:
            return

        if self.orientation == KnobOrientation.Down and self._led_set in {LedSet.DownOne, LedSet.DownTwo}:
            return

        self.on_wrong_try()
