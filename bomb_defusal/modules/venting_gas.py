from bomb_defusal.modules.module import NeedyModule
from pyglet import clock


class VentingGas(NeedyModule):
    def __init__(self, position, expiration):
        super().__init__(position, expiration)

        self._question = 'VENT GAS?\nY/N'
        self._no_answer = 'VENTING\nPREVENTS\nEXPLOSIONS'

        self._text = self._question
        self._block_interaction = False

    @property
    def text(self):
        if not self._activated:
            return 'VENTING\nCOMPLETE'
        return self._text

    def yes(self):
        if self._block_interaction:
            return True
        self._reset()
        self._text = self._text + '\nYES'
        self._block_interaction = True
        clock.schedule_once(self._show_question, 1)

    def no(self):
        if self._block_interaction:
            return True
        self._text = self._text + '\nNO'
        self._block_interaction = True
        clock.schedule_once(self._show_no_answer, 1)
        clock.schedule_once(self._show_question, 3)

    def _show_question(self):
        self._block_interaction = False
        self._text = self._question

    def _show_no_answer(self):
        self._text = self._no_answer
