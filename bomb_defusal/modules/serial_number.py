from bomb_defusal.modules.module import Module


class SerialNumber(Module):
    """The serial number plate of the bomb."""
    def __init__(self, position, serial_number):
        """
        Initializes a new instance of the :class:`SerialNumber` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param str serial_number: The number itself.
        :raises ValueError: If the serial number is invalid.
        """
        super().__init__(position)
        if not any(n.isdigit() for n in serial_number):
            raise ValueError('serial_number must contain at least one digit')
        self._serial_number = serial_number

    @property
    def serial_number(self):
        """
        Get the serial number.

        :rtype: str
        """
        return self._serial_number

    @property
    def is_odd(self):
        return int(list(filter(str.isdigit, self._serial_number))[-1]) in range(1, 10, 2)

    @property
    def is_even(self):
        return int(list(filter(str.isdigit, self._serial_number))[-1]) in range(0, 10, 2)

    @property
    def has_vowel(self):
        return any(c.isalnum() and c in 'aeiou' for c in self.serial_number.lower())

    def __str__(self):
        return self._serial_number
