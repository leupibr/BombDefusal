import random

from bomb_defusal.modules.module import DisarmableModule


class WhoIsOnFirst(DisarmableModule):
    """
    This module is disarmed by pressing the buttons in the correct order. After each step, the buttons get new
    labels. The button that needs to be pressed, is determined by a lookup mask based on the major label and the
    first occurrence in the set that is behind the mask.
    """
    _lookup_mask = {
        'YES': 2, 'FIRST': 1, 'DISPLAY': 5, 'OKAY': 1, 'SAYS': 5, 'NOTHING': 2,
        '': 4, 'BLANK': 3, 'NO': 5, 'LED': 2, 'LEAD': 5, 'READ': 3,
        'RED': 3, 'REED': 4, 'LEED': 4, 'HOLD ON': 5, 'YOU': 3, 'YOU ARE': 5,
        'YOUR': 3, 'YOU\'RE': 3, 'UR': 0, 'THERE': 5, 'THEY\'RE': 4, 'THEIR': 3,
        'THEY ARE': 2, 'SEE': 5, 'C': 1, 'CEE': 5
    }
    _button_set = {
        'READY': 'YES,OKAY,WHAT,MIDDLE,LEFT,PRESS,RIGHT,BLANK,READY,NO,FIRST,UHHH,NOTHING,WAIT'.split(','),
        'FIRST': 'LEFT,OKAY,YES,MIDDLE,NO,RIGHT,NOTHING,UHHH,WAIT,READY,BLANK,WHAT,PRESS,FIRST'.split(','),
        'NO': 'BLANK,UHHH,WAIT,FIRST,WHAT,READY,RIGHT,YES,NOTHING,LEFT,PRESS,OKAY,NO,MIDDLE'.split(','),
        'BLANK': 'WAIT,RIGHT,OKAY,MIDDLE,BLANK,PRESS,READY,NOTHING,NO,WHAT,LEFT,UHHH,YES,FIRST'.split(','),
        'NOTHING': 'UHHH,RIGHT,OKAY,MIDDLE,YES,BLANK,NO,PRESS,LEFT,WHAT,WAIT,FIRST,NOTHING,READY'.split(','),
        'YES': 'OKAY,RIGHT,UHHH,MIDDLE,FIRST,WHAT,PRESS,READY,NOTHING,YES,LEFT,BLANK,NO,WAIT'.split(','),
        'WHAT': 'UHHH,WHAT,LEFT,NOTHING,READY,BLANK,MIDDLE,NO,OKAY,FIRST,WAIT,YES,PRESS,RIGHT'.split(','),
        'UHHH': 'READY,NOTHING,LEFT,WHAT,OKAY,YES,RIGHT,NO,PRESS,BLANK,UHHH,MIDDLE,WAIT,FIRST'.split(','),
        'LEFT': 'RIGHT,LEFT,FIRST,NO,MIDDLE,YES,BLANK,WHAT,UHHH,WAIT,PRESS,READY,OKAY,NOTHING'.split(','),
        'RIGHT': 'YES,NOTHING,READY,PRESS,NO,WAIT,WHAT,RIGHT,MIDDLE,LEFT,UHHH,BLANK,OKAY,FIRST'.split(','),
        'MIDDLE': 'BLANK,READY,OKAY,WHAT,NOTHING,PRESS,NO,WAIT,LEFT,MIDDLE,RIGHT,FIRST,UHHH,YES'.split(','),
        'OKAY': 'MIDDLE,NO,FIRST,YES,UHHH,NOTHING,WAIT,OKAY,LEFT,READY,BLANK,PRESS,WHAT,RIGHT'.split(','),
        'WAIT': 'UHHH,NO,BLANK,OKAY,YES,LEFT,FIRST,PRESS,WHAT,WAIT,NOTHING,READY,RIGHT,MIDDLE'.split(','),
        'PRESS': 'RIGHT,MIDDLE,YES,READY,PRESS,OKAY,NOTHING,UHHH,BLANK,LEFT,FIRST,WHAT,NO,WAIT'.split(','),
        'YOU': 'SURE,YOU ARE,YOUR,YOU\'RE,NEXT,UH HUH,UR,HOLD,WHAT?,YOU,UH UH,LIKE,DONE,U'.split(','),
        'YOU ARE': 'YOUR,NEXT,LIKE,UH HUH,WHAT?,DONE,UH UH,HOLD,YOU,U,YOU\'RE,SURE,UR,YOU ARE'.split(','),
        'YOUR': 'UH UH,YOU ARE,UH HUH,YOUR,NEXT,UR,SURE,U,YOU\'RE,YOU,WHAT?,HOLD,LIKE,DONE'.split(','),
        'YOU\'RE': 'YOU,YOU\'RE,UR,NEXT,UH UH,YOU ARE,U,YOUR,WHAT?,UH HUH,SURE,DONE,LIKE,HOLD'.split(','),
        'UR': 'DONE,U,UR,UH HUH,WHAT?,SURE,YOUR,HOLD,YOU\'RE,LIKE,NEXT,UH UH,YOU ARE,YOU'.split(','),
        'U': 'UH HUH,SURE,NEXT,WHAT?,YOU\'RE,UR,UH UH,DONE,U,YOU,LIKE,HOLD,YOU ARE,YOUR'.split(','),
        'UH HUH': 'UH HUH,YOUR,YOU ARE,YOU,DONE,HOLD,UH UH,NEXT,SURE,LIKE,YOU\'RE,UR,U,WHAT?'.split(','),
        'UH UH': 'UR,U,YOU ARE,YOU\'RE,NEXT,UH UH,DONE,YOU,UH HUH,LIKE,YOUR,SURE,HOLD,WHAT?'.split(','),
        'WHAT?': 'YOU,HOLD,YOU\'RE,YOUR,U,DONE,UH UH,LIKE,YOU ARE,UH HUH,UR,NEXT,WHAT?,SURE'.split(','),
        'DONE': 'SURE,UH HUH,NEXT,WHAT?,YOUR,UR,YOU\'RE,HOLD,LIKE,YOU,U,YOU ARE,UH UH,DONE'.split(','),
        'NEXT': 'WHAT?,UH HUH,UH UH,YOUR,HOLD,SURE,NEXT,LIKE,DONE,YOU ARE,UR,YOU\'RE,U,YOU'.split(','),
        'HOLD': 'YOU ARE,U,DONE,UH UH,YOU,UR,SURE,WHAT?,YOU\'RE,NEXT,HOLD,UH HUH,YOUR,LIKE'.split(','),
        'SURE': 'YOU ARE,DONE,LIKE,YOU\'RE,YOU,HOLD,UH HUH,UR,SURE,U,WHAT?,NEXT,YOUR,UH UH'.split(','),
        'LIKE': 'YOU\'RE,NEXT,U,UR,HOLD,DONE,UH UH,WHAT?,UH HUH,YOU,LIKE,SURE,YOU ARE,YOUR'.split(',')
    }
    _possible_buttons = set(v for s in _button_set.values() for v in s)

    def __init__(self, position, disarmed, buttons):
        """
        Initializes a new instance of the :class:`WhoIsOnFirst` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Flag that indicates the initial disarmed status of the module.
        :param list[str] buttons: Labels that should be pressed in their correct order.
        """
        super().__init__(position, disarmed)

        if len(buttons) != 4:
            raise ValueError('There must be exactly four buttons to press.')

        if not all(b in WhoIsOnFirst._possible_buttons for b in buttons):
            raise ValueError('Invalid button')

        self._press_order = buttons

        self._button_to_press = None
        self._label, self._buttons = self._next_step()

    @property
    def buttons(self):
        """
        Gets the actual buttons

        :rtype: str
        """
        return self._buttons

    @property
    def disarm_stage(self):
        """
        Gets the actual disarm stage from 0 (no action yet) to 4 (disarmed).

        :rtype: int
        """
        return 4 if self._disarmed else 3 - len(self._press_order)

    @property
    def label(self):
        """
        Gets the actual label.

        :rtype: str
        """
        return self._label

    def press(self, label):
        """
        Press on the given label.

        :param str label: Label to be pressed
        """
        if label != self._button_to_press:
            self.on_wrong_try()
            return

        if self._press_order:
            self._label, self._buttons = self._next_step()
            return

        self._disarmed = True

    def _next_step(self):
        """
        Calculates the next step including the mask and the new buttons.

        :returns: The major label and a list of buttons
        :rtype: tuple(str, list[str])
        """
        self._button_to_press = self._press_order.pop(0)
        possible_sets = {k: v for k, v in WhoIsOnFirst._button_set.items() if self._button_to_press in v}
        chosen_set = possible_sets[random.choice(list(possible_sets.keys()))]

        disallowed_buttons = set(
            chosen_set[:chosen_set.index(self._button_to_press)] + [self._button_to_press])
        allowed_buttons = list(self._possible_buttons - disallowed_buttons)

        lookup_mask = random.choice(list(WhoIsOnFirst._lookup_mask.keys()))

        random.shuffle(allowed_buttons)
        buttons = allowed_buttons[:6]
        buttons[WhoIsOnFirst._lookup_mask[lookup_mask]] = self._button_to_press

        return lookup_mask, buttons
