import pytest
from mock import mock

from bomb_defusal.modules.memory import Memory


class TestMemory(object):
    @staticmethod
    def _assert_disarm_success(subject, order):
        subject.on_wrong_try = mock.MagicMock()
        TestMemory._press(subject, order)
        assert subject.disarmed
        subject.on_wrong_try.assert_not_called()

    @staticmethod
    def _assert_disarm_failure(subject, order):
        subject.on_wrong_try = mock.MagicMock()
        TestMemory._press(subject, order)
        assert not subject.disarmed
        subject.on_wrong_try.assert_called()

    @staticmethod
    def _press(subject, order):
        labels = []
        for p in order.split():
            if p[0] == 'l':
                # get label from history
                p = str(subject.buttons.index(labels[int(p[1])]))
            if p[0] == '!':
                # get another label from history
                history_pos = int(labels[int(p[1])])
                p = str(subject.buttons.index(str(((history_pos+1) % 4)+1)))

            press_at = int(p[-1])
            labels.append(subject.buttons[press_at])
            subject.press(press_at)

    def test_label(self):
        assert '4' == Memory(None, False, list('42314')).label

    @pytest.mark.parametrize('labels', [
        '1123', '123432', '12345'
    ])
    def test_invalid_flash_order(self, labels):
        with pytest.raises(ValueError):
            Memory(None, False, list(labels))

    @pytest.mark.parametrize('labels,press_code', [
        ('12341', '1 1 2 1 l0'),
        ('43214', '3 0 l0 3 l2')
    ])
    def test_correct_order(self, labels, press_code):
        subject = Memory(None, False, list(labels))
        self._assert_disarm_success(subject, press_code)

    @pytest.mark.parametrize('labels,press_code', [
        ('12341', '1 2'),
        ('43214', '3 0 l0 3 !2')
    ])
    def test_invalid_order(self, labels, press_code):
        subject = Memory(None, False, list(labels))
        self._assert_disarm_failure(subject, press_code)
