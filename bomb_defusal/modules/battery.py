from bomb_defusal.modules.module import Module


class Battery(Module):
    """
    Module, that indicates one or multiple batteries. Some of the other module's disarm procedure is might
    depended on total number of batteries.
    """
    def __init__(self, position, units):
        """Initializes a new instance of the :class:`Battery` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param int units: Number of batteries within this module.
        """
        super().__init__(position)
        if units not in range(1, 3):
            raise ValueError('only two or three units are allowed')
        self._units = units

    @property
    def units(self):
        """
        Gets the number of batteries in this module

        :return: Battery count
        :rtype: int
        """
        return self._units
