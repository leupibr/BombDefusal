from pyglet.gl import *

from bomb_defusal.view import components
from bomb_defusal.view.components import Led, Static
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.drawing import set_material_color
from bomb_defusal.view.utils.helper import colors


class Button(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`SimonSays` class.

        :param bomb_defusal.modules.Button model: Model of the button module
        """
        super().__init__(model, static='resources/button.obj')

        self._button = components.Button((-.2, -.08, .095), static='resources/components/button2.obj',
                                         foreground=(255, 255, 255), text_offset=.11, text_height=.15)

        self._button.text = self._model.subject
        self._leds = [Led((.5, 0, z)) for z in [.55, .4, .25, .1]]
        self._lid = Static((-.2, 0, 0), static='resources/components/lid.obj')

        # noinspection PyProtectedMember
        set_material_color(self._button._model, colors.get(self._model.button_color, (0, 0, 0, 1)))

    def hit_test(self, position, button, modifiers):
        """Tests the elements of the button module for mouse clicks"""
        if button != 1:
            return None, None, None

        distance = self._lid.hit_test(*position)
        if distance:
            return self, 'lid', distance

        distance = self._button.hit_test(*position)
        if distance:
            return self, 'button', distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        """Executes the hit action of the selected button element"""
        if tag == 'lid':
            if self._model.is_lid_open:
                self._model.close_lid()
            else:
                self._model.open_lid()
            return

        if tag == 'button':
            self._model.hold()
            self._button.press()

    def hit_release(self, tag, button, modifiers):
        """Releases the button if pressed"""
        if tag != 'button':
            return
        self._model.release()
        self._button.release()

    def on_draw(self):
        """Draws the button module"""
        self._draw_strip()
        self._button.draw()
        self._draw_lid()

    def _draw_lid(self):
        """Draws the lid"""
        glPushMatrix()

        glTranslatef(0, 0, -.44)
        if self._model.is_lid_open:
            glRotatef(-110, 1, 0, 0)
        self._lid.draw()

        glPopMatrix()

    def _draw_strip(self):
        """Draw the colored strip"""
        color = colors.get(self._model.strip_color, (.2, .2, .2, 1))
        for led in self._leds:
            led.color = color if not self._model.disarmed else (0, .75, 0, 1)
            led.draw()
