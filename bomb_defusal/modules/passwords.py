import random
import string

from bomb_defusal.modules.module import DisarmableModule


class Passwords(DisarmableModule):
    passwords = {'about', 'after', 'again', 'below', 'could', 'every', 'first', 'found', 'great', 'house',
                 'large', 'learn', 'never', 'other', 'place', 'plant', 'point', 'right', 'small', 'sound',
                 'spell', 'still', 'study', 'their', 'there', 'these', 'thing', 'think', 'three', 'water',
                 'where', 'which', 'world', 'would', 'write'}

    def __init__(self, position, disarmed, password):
        super().__init__(position, disarmed)

        if password not in Passwords.passwords:
            raise ValueError('Invalid password provided')

        self._charsets = list(Passwords._create_charsets(password))
        self._positions = [0] * 5

    @property
    def setting(self):
        return ''.join([self._charsets[i][self._positions[i]] for i in range(0, 5)])

    def rotate_up(self, position):
        if self.disarmed:
            return

        if self._positions[position] == 0:
            self._positions[position] = len(self._charsets[position])
        self._positions[position] -= 1

    def rotate_down(self, position):
        if self.disarmed:
            return

        if self._positions[position] == len(self._charsets[position]) - 1:
            self._positions[position] = -1
        self._positions[position] += 1

    def submit(self):
        if self.setting not in Passwords.passwords:
            self.on_wrong_try()
            return

        self._disarmed = True

    @staticmethod
    def _create_charsets(password):
        for c in password:
            yield Passwords._create_charset(c)

    @staticmethod
    def _create_charset(char):
        letters = list(set(string.ascii_lowercase) - set(char))
        random.shuffle(letters)
        charset = [char] + letters[:4]
        random.shuffle(charset)
        return ''.join(charset)
