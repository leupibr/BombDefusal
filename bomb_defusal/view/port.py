from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import load_model


class Port(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Port` class.

        :param bomb_defusal.modules.Port model: Model of the port module
        """
        super().__init__(model, base=None)

        ports = load_model('resources/components/ports.obj')
        port_name = self._model.port.value.replace(' ', '_').replace('/', '_')
        self._static = ports.meshes[port_name]
