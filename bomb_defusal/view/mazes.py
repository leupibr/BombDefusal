from pyglet import clock
from pyglet.gl import glPushMatrix, glPopMatrix, glTranslatef, glRotatef

from bomb_defusal.view.components import Button, Static
from bomb_defusal.view.module import Module


class Mazes(Module):
    inactive = dict(diffuse=(0, .1, .5, 1), ambient=(0, .3, .5, 1), specular=(0, .3, .5, 1), emissive=(0, 0, 0, 1))
    active = dict(diffuse=(1, 1, 1, 1), ambient=(1, 1, 1, 1), specular=(1, 1, 1, 1), emissive=(1, 1, 1, 1))
    alert = dict(diffuse=(.5, 0, 0, 1), ambient=(0, 0, 0, 1), specular=(0, 0, 0, 1), emissive=(1, 0, 0, 1))

    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Mazes` class.

        :param bomb_defusal.model.Mazes model: Model of the mazes module
        """
        super().__init__(model, static='resources/maze.obj')

        self._target = Static((0, 0, 0), static='resources/components/triangle.obj')
        self._statics = [
            Static(self._get_position(self._model.marker[0]), static='resources/components/ring.obj'),
            Static(self._get_position(self._model.marker[1]), static='resources/components/ring.obj'),
        ]

        self._buttons = dict(
            U=Button((0, 0, -.5), static='resources/components/morse_button.obj', rotate_y=270, scale=(1.5, 1, 1.5)),
            R=Button((.5, 0, 0), static='resources/components/morse_button.obj', rotate_y=180, scale=(1.5, 1, 1.5)),
            D=Button((0, 0, .5), static='resources/components/morse_button.obj', rotate_y=90, scale=(1.5, 1, 1.5)),
            L=Button((-.5, 0, 0), static='resources/components/morse_button.obj', scale=(1.5, 1, 1.5)),
        )

        self._dots = {(x, y): Static(self._get_position((x, y)), static='resources/components/dot.obj')
                      for x in range(6) for y in range(6)}
        [dot.set_color(**Mazes.inactive) for dot in self._dots.values()]
        self._dots[self._model.cursor].set_color(**Mazes.active)

        self._target_rotate = 0
        self._alert = 0
        self._model.on_wrong_try += self._on_wrong_try
        clock.schedule_interval(self._tick, .1)

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        for direction, button in self._buttons.items():
            distance = button.hit_test(*position)
            if distance:
                return self, direction, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        self._buttons[tag].press()
        old_pos = self._model.cursor
        self._model.move(tag)
        self._dots[old_pos].set_color(**Mazes.inactive)
        self._dots[self._model.cursor].set_color(**Mazes.active)

    def hit_release(self, tag, button, modifiers):
        self._buttons[tag].release()

    def on_draw(self):
        [static.draw() for static in self._statics]
        [button.draw() for button in self._buttons.values()]
        [dot.draw() for pos, dot in self._dots.items() if pos != self._model.target]

        glPushMatrix()
        glTranslatef(*self._get_position(self._model.target))
        glRotatef(self._target_rotate, 0, 1, 0)
        self._target.draw()
        glPopMatrix()

    def _get_position(self, pos):
        return -.3 + (.12 * pos[0]), 0, -.3 + (.12 * pos[1])

    def _on_wrong_try(self):
        self._alert += .5
        [dot.set_color(**Mazes.alert) for pos, dot in self._dots.items() if pos != self._model.cursor]

    def _tick(self, dt):
        if self._alert > 0:
            self._alert = max(self._alert - dt, 0)
            if self._alert <= 0:
                [dot.set_color(**Mazes.inactive) for pos, dot in self._dots.items() if pos != self._model.cursor]

        self._target_rotate = (self._target_rotate + dt * 90) % 360
