from bomb_defusal.view.components import Led, Button, Lcd
from bomb_defusal.view.module import Module


class WhoIsOnFirst(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`WhoIsOnFirst` class.

        :param bomb_defusal.modules.WhoIsOnFirst model: Model of the module
        """
        super().__init__(model)

        self._display = Lcd((-.15, 0, -.4))
        self._buttons = [Button((-.4 + (i % 2) * .5, 0, (i // 2) * .25), hit_on_box=True) for i in range(6)]
        self._leds = [Led((.5, 0, z)) for z in [.55, .4, .25, .1]]

        self._update_stage()
        self._update_display()
        self._update_buttons()

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        for index, button in enumerate(self._buttons):
            distance = button.hit_test(*position)
            if distance:
                return self, index, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        self._buttons[tag].press()
        self._model.press(self._model.buttons[tag])
        self._update_stage()

    def hit_release(self, tag, button, modifiers):
        self._buttons[tag].release()
        self._update_display()
        self._update_buttons()

    def on_draw(self):
        """Draw the 3D view of the module"""
        [led.draw() for led in self._leds]
        [button.draw() for button in self._buttons]
        self._display.draw()

    def _update_stage(self):
        on = (1, 0, 0, 1) if self._model.disarm_stage < 4 else (0, .75, 0, 1)
        off = (.2, 0, 0, 1)

        for i in range(4):
            self._leds[i].color = on if i < self._model.disarm_stage else off
            self._leds[i].active = i < self._model.disarm_stage

    def _update_buttons(self):
        for i, text in enumerate(self._model.buttons):
            self._buttons[i].text = text

    def _update_display(self):
        self._display.text = self._model.label
