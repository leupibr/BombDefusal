import itertools

import sys
from pyglet.gl import *
from pywavefront import Wavefront
from pywavefront.material import Material
from pywavefront.mesh import Mesh

from bomb_defusal.view.utils.vector import Vector

colors = {
    'Blue': [0, 0, 1, 1],
    'Yellow': [1, 1, 0, 1],
    'Green': [0, .5, 0, 1],
    'Red': [1, 0, 0, 1],
    'White': [1, 1, 1, 1],
    'Black': [.1, .1, .1, 1],
}


def vector(*args):
    """ Define a simple function to create ctypes arrays of floats """
    # noinspection PyCallingNonCallable,PyTypeChecker
    return (GLfloat * len(args))(*args)


def matrix4x4(*args):
    # noinspection PyCallingNonCallable,PyTypeChecker
    return (GLdouble * 16)(*args)


def int_vector(*args):
    # noinspection PyCallingNonCallable,PyTypeChecker
    return (GLint * 4)(*args)


def load_model(file_path):
    """
    Loads a wavefront obj file and fixes the ambient color (blender export issue).
    The method replaces the loaded ambient color of the model with the diffuse color. This is required because
    the ambient value is set to a invalid value by the blender export script.

    :param path file_path: Path to the wavefront obj file to load
    :returns: 3D model representing the loaded wavefront obj file
    :rtype: pywavefront.Wavefront
    """
    model = Wavefront(file_path)
    for material in model.materials.values():
        material.ambient = material.diffuse
    return model


def grouper(iterable, n, fillvalue=None):
    """ Collect data into fixed-length chunks of blocks """
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


def get_hit_distance(vertices, mouse, model_matrix):
    """
    Gets the distance of the closest hit on a mesh or None if the mesh is not hit

    :param Union[Wavefront, Mesh, list] vertices: Mesh to get the distance of the closest hit
    :param mouse: Mouse position on the screen
    :param model_matrix: Model matrix for the mesh
    :return: Distance to the closest hit or None
    """
    if isinstance(vertices, Wavefront):
        closest = None
        for m in vertices.meshes.values():
            distance = get_hit_distance(m, mouse, model_matrix)
            if closest is None or distance < closest:
                closest = distance
        return closest

    if isinstance(vertices, Mesh):
        closest = None
        for material in vertices.materials:
            distance = get_hit_distance(material, mouse, model_matrix)
            if closest is None or distance < closest:
                closest = distance
        return closest

    if isinstance(vertices, Material):
        closest = None
        distance = get_hit_distance(vertices.vertices, mouse, model_matrix)
        if closest is None or distance < closest:
            closest = distance
        return closest

    projection_matrix = matrix4x4()
    glGetDoublev(GL_PROJECTION_MATRIX, projection_matrix)

    viewport = int_vector()
    glGetIntegerv(GL_VIEWPORT, viewport)

    near = (GLdouble(), GLdouble(), GLdouble())
    gluUnProject(*mouse, 0, model_matrix, projection_matrix, viewport, *near)
    near = Vector(*[v.value for v in near])

    far = (GLdouble(), GLdouble(), GLdouble())
    gluUnProject(*mouse, 1, model_matrix, projection_matrix, viewport, *far)
    far = Vector(*[v.value for v in far])

    hit_distance = None
    ray_length = far - near

    # get triangles from vertices 3x (2x texture, 3x normal, 3x vertex) = 24
    for triangle in grouper(vertices, 24):
        n = Vector(*triangle[2:5])
        a = Vector(*triangle[5:8])
        b = Vector(*triangle[13:16])
        c = Vector(*triangle[21:24])

        dot_length = n * ray_length
        # if dot_length <= 0:
            # ray is parallel to plane
            # continue

        distance = n * (a - near) / dot_length
        if distance < 0 or distance > 1:
            # plane is beyond the ray we consider
            continue

        p = near + distance * ray_length
        n1 = (b - a).cross(p - a)
        n2 = (c - b).cross(p - b)
        n3 = (a - c).cross(p - c)
        if n * n1 >= 0. and n * n2 >= 0. and n * n3 >= 0.:
            # We have found on eof the triangle that intersects the line/ray
            if hit_distance is None or hit_distance > distance:
                hit_distance = distance

    return hit_distance


def get_bounds(vertices):
    """
    Calculates the bounding box for the given 3D object

    :param Union[Wavefront, Mesh, list] vertices: 3D object to calculate the bounding box for
    :return: Minimum and maximum vertex of the 3D object
    :rtype: Tuple[list, list]
    """

    def min_max(minimum, maximum, *vertices):
        for vertex in vertices:
            minimum = (min(minimum[0], vertex[0]), min(minimum[1], vertex[1]), min(minimum[2], vertex[2]))
            maximum = (max(maximum[0], vertex[0]), max(maximum[1], vertex[1]), max(maximum[2], vertex[2]))
        return minimum, maximum

    minsize = -sys.maxsize - 1
    minimum = (sys.maxsize, sys.maxsize, sys.maxsize)
    maximum = (minsize, minsize, minsize)

    if isinstance(vertices, Wavefront):
        for m in vertices.meshes.values():
            minimum, maximum = min_max(minimum, maximum, *get_bounds(m))
        return minimum, maximum

    if isinstance(vertices, Mesh):
        for material in vertices.materials:
            minimum, maximum = min_max(minimum, maximum, *get_bounds(material))
        return minimum, maximum

    if isinstance(vertices, Material):
        minimum, maximum = min_max(minimum, maximum, *get_bounds(vertices.vertices))
        return minimum, maximum

    for vertex in grouper(vertices, 8):
        minimum, maximum = min_max(minimum, maximum, vertex[5:8])

    return minimum, maximum


def create_box(minimum, maximum):
    """
    Creates the vertices representing the box of the minimum and maximum vertices

    :param minimum: Vertex of the absolute minimum of all 3 axes
    :param maximum: Vertex of the absolute maximum of all 3 axes
    :return: Vertices in UV, NORM, VERTEX format of the box defined with the given bounds
    """
    vertices = [
        (minimum[0], minimum[1], minimum[2]),
        (maximum[0], minimum[1], minimum[2]),
        (maximum[0], maximum[1], minimum[2]),
        (minimum[0], maximum[1], minimum[2]),
        (minimum[0], minimum[1], maximum[2]),
        (maximum[0], minimum[1], maximum[2]),
        (maximum[0], maximum[1], maximum[2]),
        (minimum[0], maximum[1], maximum[2]),
    ]
    return create_box_from_vertices(vertices)


def create_box_from_vertices(vertices):
    faces = [(1, 0, 3), (1, 3, 2),
             (4, 5, 6), (4, 6, 7),
             (4, 7, 3), (4, 3, 0),
             (6, 5, 1), (6, 1, 2),
             (5, 4, 0), (5, 0, 1),
             (7, 6, 2), (7, 2, 3)]

    box = []
    for face in faces:
        u = Vector(*vertices[face[1]]) - Vector(*vertices[face[0]])
        v = Vector(*vertices[face[2]]) - Vector(*vertices[face[1]])
        normal = Vector(
            u[1] * v[2] - u[2] * v[1],
            u[2] * v[0] - u[0] * v[2],
            u[0] * v[1] - u[1] * v[0]).normalize().values

        for index in face:
            box.extend([0, 0])
            box.extend(normal)
            box.extend(vertices[index])

    return box


def draw_box(vertices, color=(1, 1, 1, .9), emission=None, face_type=GL_TRIANGLES):
    glMaterialfv(GL_FRONT, GL_DIFFUSE, vector(*color))
    glMaterialfv(GL_FRONT, GL_AMBIENT, vector(*color))
    glMaterialfv(GL_FRONT, GL_SPECULAR, vector(*color))
    if emission:
        glMaterialfv(GL_FRONT, GL_EMISSION, vector(*emission))

    gl_floats = vector(*vertices)
    faces_count = len(vertices) // 8
    glInterleavedArrays(GL_T2F_N3F_V3F, 0, gl_floats)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    glDrawArrays(face_type, 0, faces_count)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
