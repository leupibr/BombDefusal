from bomb_defusal.modules.module import DisarmableModule


class SimonSays(DisarmableModule):
    _press_map = {
        True: {
            0: {'Red': 'Blue', 'Blue': 'Red', 'Green': 'Yellow', 'Yellow': 'Green'},
            1: {'Red': 'Yellow', 'Blue': 'Green', 'Green': 'Blue', 'Yellow': 'Red'},
            2: {'Red': 'Green', 'Blue': 'Red', 'Green': 'Yellow', 'Yellow': 'Blue'},
        },
        False: {
            0: {'Red': 'Blue', 'Blue': 'Yellow', 'Green': 'Green', 'Yellow': 'Red'},
            1: {'Red': 'Red', 'Blue': 'Blue', 'Green': 'Yellow', 'Yellow': 'Green'},
            2: {'Red': 'Yellow', 'Blue': 'Green', 'Green': 'Blue', 'Yellow': 'Red'},
        }
    }

    _allowed_colors = {'Red', 'Green', 'Blue', 'Yellow'}

    def __init__(self, position, disarmed, flash_order, bomb):
        """
        Initializes a new instance of the :class:`SimonSays` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Value that indicates whether the module is disarmed or not.
        :param list[str] flash_order: Order of the colors to push.
        :param Bomb bomb: Bomb which this module is mounted on.
        """
        super().__init__(position, disarmed)

        self._bomb = bomb

        if len(flash_order) != 4:
            raise ValueError('There must be exact four buttons for flash order')

        if any(c not in SimonSays._allowed_colors for c in flash_order):
            raise ValueError('Invalid color in flash order')

        self._flash_order = flash_order
        self._reset_buttons()

    @property
    def flash_order(self):
        """Gets the flash order for the current step"""
        for i in range(len(self._flash_order)):
            if i * (i + 1) / 2 <= self._hit_count:
                continue
            return self._flash_order[:i]
        return self._flash_order[:] if not self.disarmed else []

    def press(self, color):
        """Presses the given color."""
        if self._disarmed:
            return

        self._hit_count += 1
        if color != self._press_order.pop(0):
            self._reset_buttons()
            self.on_wrong_try()

        if not self._press_order:
            self._disarmed = True

    def _get_press_order(self):
        """
        Calculates the press order based on the flash order
        """
        has_vowel = self._bomb.serial_number.has_vowel
        strikes = self._bomb.timer.strikes

        press_map = SimonSays._press_map[has_vowel][strikes]
        for f in self._flash_order:
            yield press_map[f]

    def _reset_buttons(self):
        """Resets the buttons to press"""
        self._hit_count = 0
        press_order = list(self._get_press_order())
        self._press_order = press_order[:1] + press_order[:2] + press_order[:3] + press_order[:]

