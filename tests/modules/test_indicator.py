import pytest

from bomb_defusal.modules import Indicator


class TestIndicator(object):
    @pytest.mark.parametrize('indication', ['SND', 'CLR', 'CAR'])
    def test_indication(self, indication):
        subject = Indicator(None, indication)
        assert indication == subject.label.name
        assert indication == str(subject)

    def test_invalid_label(self):
        with pytest.raises(KeyError):
            Indicator(None, 'INVALID')
