from datetime import timedelta
from pyglet import clock


from bomb_defusal.utils.callable_collection import CallableCollection


class Module(object):
    """Base class for every module."""
    def __init__(self, position):
        """
        Initializes a new instance of the :class:`Module` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        """
        self._orientation = position

    @property
    def position(self):
        """
        Gets the position of the module.

        :rtype: tuple[str, int]
        """
        return self._orientation


class DisarmableModule(Module):
    """Base class for every module, that can be disarmed."""
    def __init__(self, position, disarmed):
        """
        Initializes a new instance of the :class:`DisarmableModule` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Flag that indicates the initial disarmed status of the module.
        """
        super().__init__(position)

        self._disarmed = disarmed
        self.on_wrong_try = CallableCollection()

    @property
    def disarmed(self):
        """
        Gets the disarmed status of the module.

        :returns: `True` if disarmed, `False` otherwise.
        :rtype: bool
        """
        return self._disarmed


class NeedyModule(Module):
    """Base class for every module, that put a recurrent hazard to the bomb."""
    def __init__(self, position, expiration):
        """
        Initializes a new instance of the :class:`NeedyModule` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param timedelta expiration: Timedelta how long to reset the expiration time.
        """
        super().__init__(position)

        self.on_wrong_try = CallableCollection()
        self._on_time_up = lambda: self.on_wrong_try()
        self._duration = expiration
        self._expiration = expiration
        self._activated = False

    @property
    def expiration(self):
        """
        Expiration as a delta
        :rtype timedelta:
        """
        return self._expiration

    def activate(self):
        if not self._activated:
            clock.schedule_interval(self._tick, 1)
            self._activated = True

    def deactivate(self):
        self._activated = False

    def _tick(self):
        if not self._activated:
            return

        self._expiration -= timedelta(seconds=1)
        if self._expiration.total_seconds() <= 0:
            self._on_time_up()
            self._reset()

    def _reset(self):
        self._expiration = self._duration
