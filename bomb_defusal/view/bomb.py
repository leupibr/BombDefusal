from pyglet.gl import *

import bomb_defusal.bomb
from bomb_defusal import view
from bomb_defusal.view.components import Static
from bomb_defusal.view.utils.drawing import scale

Snap = 10


class Bomb(object):
    def __init__(self, bomb_model):
        """
        Initializes a new instance of the :class:`Bomb` class.

        :param bomb_defusal.bomb.Bomb bomb_model: Model of the bomb
        """
        self._bomb_model = bomb_model
        self._static = Static((0, 0, 0), static='resources/bomb.obj', hit_on_box=True)
        scale(self._static.bounding_box, .99, .99, .8)
        self._rotation = [0, 0, 0]
        self._hit = None

        self._modules = []
        for model in self._bomb_model.modules:
            self._modules.append(Bomb.create_view(model))

    @property
    def rotation(self):
        """ Gets the current rotation of the bomb """
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        """ Sets the current rotation of the bomb """
        self._rotation = value

    def draw(self):
        """ Draw the bomb and all it's modules """
        glPushMatrix()
        glRotatef(self._rotation[0] // Snap * Snap, 1, 0, 0)
        glRotatef(self._rotation[1] // Snap * Snap, 0, 1, 0)
        glRotatef(self._rotation[2] // Snap * Snap, 0, 0, 1)

        self._static.draw()
        for model in self._modules:
            model.draw()

        glPopMatrix()

    def hit(self, position, button, modifiers):
        """ Hits the closest object which is located at the given position """
        closest_hit = None

        for model in self._modules:
            mod, elem, dist = model.hit_test(position, button, modifiers)
            if mod and (closest_hit is None or closest_hit[2] > dist):
                closest_hit = (mod, elem, dist)

        # prevent hit from backside by checking bomb case
        distance = self._static.hit_test(*position)
        if distance and closest_hit and distance < closest_hit[2]:
            return

        if closest_hit and closest_hit[0]:
            closest_hit[0].hit(closest_hit[1], button, modifiers)
            self._hit = closest_hit

        return closest_hit

    def hit_release(self, position, button, modifiers):
        if not self._hit:
            return False

        self._hit[0].hit_release(self._hit[1], button, modifiers)
        self._hit = None
        return True

    @staticmethod
    def create_view(model):
        """
        Creates the view for the given module model.
        We expect that the view class has exactly the same name as the model!

        :param modules.Module model: Model to create a view for
        """
        cls = getattr(view, type(model).__name__)
        return cls(model)
