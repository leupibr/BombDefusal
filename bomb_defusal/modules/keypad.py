from enum import Enum, unique

from bomb_defusal.modules.module import DisarmableModule


@unique
class KeypadChar(Enum):
    AELIG = 'aelig'
    ALVEOLAR = 'alveolar'
    ARCHKOPPA = 'archkoppa'
    BE = 'be'
    COPY = 'copy'
    IBIGYUS = 'ibigyus'
    IQUEST = 'iquest'
    KAI = 'kai'
    KOMIDZJE = 'komidzje'
    KOPPA = 'koppa'
    KSI = 'ksi'
    LAMBDA = 'lambda'
    OMEGA = 'omega'
    OMEGAPSILI = 'omegapsili'
    PHI = 'phi'
    PILCROW = 'pilcrow'
    PSI = 'psi'
    RSIGMADOT = 'rsigmadot'
    SHORTI = 'shorti'
    SIGMADOT = 'sigmadot'
    STAR = 'star'
    STARF = 'starf'
    TEHRING = 'tehring'
    YAT = 'yat'
    YUS = 'yus'
    ZEDIA = 'zedia'
    ZHE = 'zhe'

    def __str__(self):
        return self.value


class Keypad(DisarmableModule):
    """
    The keypad module gets disarmed by pressing the buttons on the screen in the right sequence. The Sequence is
    given by the set that contains all of the buttons in it.
    """
    _sets = [
        [KeypadChar.ARCHKOPPA,
         KeypadChar.YUS,
         KeypadChar.LAMBDA,
         KeypadChar.KOPPA,
         KeypadChar.IBIGYUS,
         KeypadChar.KAI,
         KeypadChar.RSIGMADOT],

        [KeypadChar.ZEDIA,
         KeypadChar.ARCHKOPPA,
         KeypadChar.RSIGMADOT,
         KeypadChar.PHI,
         KeypadChar.STAR,
         KeypadChar.KAI,
         KeypadChar.IQUEST],

        [KeypadChar.COPY,
         KeypadChar.OMEGAPSILI,
         KeypadChar.PHI,
         KeypadChar.ZHE,
         KeypadChar.KOMIDZJE,
         KeypadChar.LAMBDA,
         KeypadChar.STAR],

        [KeypadChar.BE,
         KeypadChar.PILCROW,
         KeypadChar.YAT,
         KeypadChar.IBIGYUS,
         KeypadChar.ZHE,
         KeypadChar.IQUEST,
         KeypadChar.TEHRING],

        [KeypadChar.PSI,
         KeypadChar.TEHRING,
         KeypadChar.YAT,
         KeypadChar.SIGMADOT,
         KeypadChar.PILCROW,
         KeypadChar.KSI,
         KeypadChar.STARF],

        [KeypadChar.BE,
         KeypadChar.ZEDIA,
         KeypadChar.ALVEOLAR,
         KeypadChar.AELIG,
         KeypadChar.PSI,
         KeypadChar.SHORTI,
         KeypadChar.OMEGA]
    ]

    _valid_signs = set(c for s in _sets for c in s)

    def __init__(self, position, disarmed, buttons):
        """
        Initializes a new instance of the :class:`Keypad` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Flag that indicates the initial disarmed status of the module.
        :param list[KeypadChar] buttons: List of sign-names, indicating all four buttons.

        :raises ValueError: If one of the attributes is invalid.
        """
        super().__init__(position, disarmed)

        if len(buttons) != 4:
            raise ValueError('There must be exactly four buttons')

        if set(buttons) - Keypad._valid_signs:
            raise ValueError('One of the buttons has an invalid sign')

        self._buttons = buttons
        self._set = self._find_set()
        self._order = sorted(self._buttons, key=self._set.index)

    @property
    def buttons(self):
        """
        Gets the actual displayed buttons

        :rtype: list[KeypadChar]
        """
        return self._buttons

    def press_position(self, index):
        """
        Presses the button at the given position

        :param int index: Zero based index of the button.
        """
        self.press(self._buttons[index])

    def press(self, button):
        """
        Press the button

        :param KeypadChar button: Name of the sign on the button that has been pressed
        """
        if button != self._order.pop(0):
            self.on_wrong_try()
            return

        if not self._order:
            self._disarmed = True

    def _find_set(self):
        """
        Finds the set, that matches all buttons.

        :rtype: list[KeypadChar]
        :raises AttributeError: If no set with all buttons has been found
        """
        for i, set_ in enumerate(Keypad._sets):
            if not set(self._buttons) - set(set_):
                return set_
        raise ValueError('No set found for the given buttons')
