from pyglet import clock

from bomb_defusal.view.components import Lcd, Led, Button
from bomb_defusal.view.module import Module
from bomb_defusal.view.utils.helper import colors


class Passwords(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Passwords` class.

        :param bomb_defusal.modules.Passwords model: Model of the passwords module
        """
        super().__init__(model)

        self._display = Lcd((-.2, 0, -.1), static='resources/components/lcd2.obj',
                            text_font='resources/fonts/pixel.ttf',
                            foreground=(0, 0, 0), text_height=.18, text_offset=-.04)
        self._transmit = Button((.4, 0, .5), label='TX')

        self._buttons = []
        for i in range(5):
            self._buttons.append(Button((-.5 + i * .15, 0, -.45), static='resources/components/button3.obj',
                                        rotate_y=180))
            self._buttons.append(Button((-.5 + i * .15, 0, .25), static='resources/components/button3.obj'))

        self._update_display()
        self._set_button_colors('Yellow')

        self._alert = 0
        self._model.on_wrong_try += self._on_wrong_try
        clock.schedule_interval(self._tick, .1)

    def hit_test(self, position, button, modifiers):
        if button != 1:
            return None, None, None

        distance = self._transmit.hit_test(*position)
        if distance:
            return self, 'tx', distance

        for index, button in enumerate(self._buttons):
            distance = button.hit_test(*position)
            if distance:
                return self, index, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        if tag == 'tx':
            self._model.submit()
            return self._transmit.press()

        self._buttons[tag].press()
        if tag % 2 == 0:
            self._model.rotate_down(tag // 2)
        else:
            self._model.rotate_up(tag // 2)

    def hit_release(self, tag, button, modifiers):
        if tag == 'tx':
            return self._transmit.release()

        self._buttons[tag].release()
        self._update_display()

    def on_draw(self):
        self._display.draw()
        [button.draw() for button in self._buttons]
        self._transmit.draw()

    def _on_wrong_try(self):
        self._alert += .5
        self._set_button_colors('Red', 'Red')

    def _set_button_colors(self, color, emissive=None):
        for button in self._buttons:
            button.set_color(ambient=colors[color], emissive=colors[emissive] if emissive else (0, 0, 0),
                             material='ButtonLed')

    def _update_display(self):
        self._display.text = str(self._model.setting).upper()

    def _tick(self, dt):
        if self._alert > 0:
            self._alert = max(self._alert - dt, 0)
            if self._alert <= 0:
                self._set_button_colors('Yellow')
                for button in self._buttons:
                    button.set_color(ambient=colors['Yellow'], emissive=(0, 0, 0), material='ButtonLed')
