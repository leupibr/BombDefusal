import pytest
from mock import mock

from bomb_defusal.bomb import Bomb
import bomb_defusal.modules as modules


class TestBomb(object):
    def test_modules(self):
        subject = Bomb()

        mods = [
            modules.SerialNumber(('Left', 1), '123f23'),
            modules.Indicator(('Right', 2), 'SND'),
            modules.Wire(('Front', 3), False, ['Blue', 'Red', 'Green'], subject)
        ]
        subject.add_module(*mods)
        assert mods == subject.modules

    def test_add_module_already_used(self):
        subject = Bomb()
        subject.add_module(modules.SerialNumber(('Left', 1), '123f23'))
        with pytest.raises(AttributeError):
            subject.add_module(modules.Indicator(('Left', 1), 'SND'))

    def test_serial_number(self):
        subject = Bomb()
        subject.add_module(modules.SerialNumber(('Left', 1), 'ec126457'))
        assert 'ec126457' == str(subject.serial_number)

    @pytest.mark.parametrize('mods,count', [
        ([modules.Battery(('Left', 1), 1)], 1),
        ([modules.Battery(('Left', 1), 2)], 2),
        ([modules.Battery(('Left', 1), 1), modules.Battery(('Right', 1), 1)], 2),
        ([modules.Battery(('Left', 1), 2), modules.Battery(('Right', 1), 2)], 4),
    ])
    def test_battery_count(self, mods, count):
        subject = Bomb()
        subject.add_module(*mods)
        assert count == subject.battery_count

    def test_indicators(self):
        subject = Bomb()

        mods = [
            modules.SerialNumber(('Left', 1), '123f23'),
            modules.Indicator(('Right', 2), 'SND'),
            modules.Indicator(('Right', 1), 'CLR'),
            modules.Wire(('Front', 3), False, ['Blue', 'Red', 'Green'], subject)
        ]
        subject.add_module(*mods)
        assert mods[1:3] == list(subject.indicators)

    def test_timer(self):
        subject = Bomb()

        mods = [
            modules.Indicator(('Right', 2), 'SND'),
            modules.CountdownTimer(('Left', 1), 600),
            modules.Indicator(('Right', 1), 'CLR'),
            modules.Wire(('Front', 3), False, ['Blue', 'Red', 'Green'], subject)
        ]
        subject.add_module(*mods)
        assert mods[1] == subject.timer

    def test_detonate_due_to_timer(self):
        subject = Bomb()
        subject.on_detonate = mock.MagicMock()

        timer = modules.CountdownTimer(('Left', 1), 1)
        subject.add_module(timer)
        timer._on_step()
        subject.on_detonate.assert_called_once()

    def test_detonate_due_to_strikes(self):
        subject = Bomb()
        subject.on_detonate = mock.MagicMock()

        timer = modules.CountdownTimer(('Left', 1), 600)
        subject.add_module(timer)

        for i in range(3): timer.strike()
        subject.on_detonate.assert_called_once()
