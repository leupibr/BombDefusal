from bomb_defusal.view.module import Module


class Charge(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Charge` class.

        :param bomb_defusal.modules.Charge model: Model of the charge module
        """
        super().__init__(model, static='resources/charge.obj')
