from bomb_defusal.view.components import Button, ImageTexture
from bomb_defusal.view.module import Module


class Keypad(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Keypad` class.

        :param bomb_defusal.modules.Keypad model: Model of the keypad module
        """
        super().__init__(model)

        self._buttons = {}
        for i, position in enumerate([(-.2, 0, -.2), (.2, 0, -.2), (-.2, 0, .2), (.2, 0, .2)]):
            label = self._model.buttons[i]
            label_uv = self._find_uv(label)

            button = Button(position, scale=(.8, 1, 1.7), name=label, hit_on_box=True)
            button.texture = ImageTexture((position[0], .01, position[2]), 'resources/keypad.png', scale=(.3, 1, .25))
            button.texture.uv = [
                (label_uv[0] / 6, (label_uv[1] + .9) / 7),
                ((label_uv[0] + 1) / 6, (label_uv[1] + .9) / 7),
                ((label_uv[0] + 1) / 6, label_uv[1] / 7),
                (label_uv[0] / 6, label_uv[1] / 7)]

            self._buttons[label] = button

    def hit_test(self, position, button, modifiers):
        """Tests the buttons of the keypad module for hits by the mouse"""
        if button != 1:
            return None, None, None

        for name, button in self._buttons.items():
            distance = button.hit_test(*position)
            if distance:
                return self, name, distance

        return None, None, None

    def hit(self, tag, button, modifiers):
        """Performs the hit action on the selected button"""
        self._buttons[tag].press()
        self._model.press(tag)

    def hit_release(self, tag, button, modifiers):
        """Releases any pressed button"""
        self._buttons[tag].release()

    def on_draw(self):
        """Draw the keypad module"""
        [test.draw() for test in self._buttons.values()]

    def _find_uv(self, button):
        """Find the UV coordinates of the texture for the button"""
        # noinspection PyProtectedMember
        for i, column in enumerate(self._model._sets):
            for j, char in enumerate(column):
                if char == button:
                    return i, j
        return 0, 0
