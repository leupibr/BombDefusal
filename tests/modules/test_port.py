import pytest

from bomb_defusal.modules import Port, PortType


class TestPort(object):
    @pytest.mark.parametrize('port', [PortType.DVI_D, PortType.SERIAL])
    def test_indication(self, port):
        subject = Port(None, port)
        assert port == subject.port

    def test_invalid_port(self):
        with pytest.raises(ValueError):
            Port(None, 'INVALID')
