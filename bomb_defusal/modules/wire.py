from bomb_defusal.modules import PortType
from bomb_defusal.modules.module import DisarmableModule


class Wire(DisarmableModule):
    def __init__(self, position, disarmed, wires, bomb):
        """
        Initializes a new instance of the :class:`Wire` class.

        :param tuple[str, int] position: Tuple that describes the modules position.
        :param bool disarmed: Tuple that indicates whether the module is disarmed or not.
        :param list[str] wires: List of wire colors.
        :param bomb_defusal.bomb.Bomb bomb: Reference to the whole bomb to get timer, indicators and battery information.
        """
        super().__init__(position, disarmed)

        if len(wires) not in range(3, 7):
            raise ValueError('There must be three to 6 wires.')
        self._wires = wires
        self._bomb = bomb
        self._cut_strategies = {
            3: self._cut_three,
            4: self._cut_four,
            5: self._cut_five,
            6: self._cut_six
        }

    @property
    def wires(self):
        return self._wires

    def cut(self, position):
        """Cuts the wire at the given position."""
        self._cut_strategies[len(self._wires)](position)
        if not self._disarmed:
            self.on_wrong_try()

    def _cut_three(self, position):
        """
        If there are no red wires, cut the second wire.
        Otherwise, if the last wire is white, cut the last wire.
        Otherwise, if there is more than one blue wire, cut the last blue wire.
        Otherwise, cut the last wire.
        """
        if 'Red' not in self._wires:
            self._disarmed = position == 2
        elif self._wires[-1] == 'White':
            self._disarmed = position == 3
        elif self._wires.count('Blue') > 1:
            self._disarmed = position == self._last_pos_of('Blue')
        else:
            self._disarmed = position == 3

    def _cut_four(self, position):
        """
        If there is more than one red wire and the last digit of the serial number is odd, cut the last red wire.
        Otherwise, if the last wire is yellow and there are no red wires, cut the first wire.
        Otherwise, if there is exactly one blue wire, cut the first wire.
        Otherwise, if there is more than one yellow wire, cut the last wire.
        Otherwise, cut the second wire.
        """
        if self._wires.count('Red') > 1 and self._bomb.serial_number.is_odd:
            self._disarmed = position == self._last_pos_of('Red')
        elif self._wires[-1] == 'Yellow' and 'Red' not in self._wires:
            self._disarmed = position == 1
        elif self._wires.count('Blue') == 1:
            self._disarmed = position == 1
        elif self._wires.count('Yellow') > 1:
            self._disarmed = position == 4
        else:
            self._disarmed = position == 2

    def _cut_five(self, position):
        """
        If the last wire is black and the last digit of the serial number is odd, cut the fourth wire.
        Otherwise, if there is exactly one red wire and there is more than one yellow wire, cut the first wire.
        Otherwise, if there are no black wires, cut the second wire.
        Otherwise, cut the first wire.
        """
        if self._wires[-1] == 'Black' and self._bomb.serial_number.is_odd:
            self._disarmed = position == 4
        elif self._wires.count('Red') == 1 and self._wires.count('Yellow') > 1:
            self._disarmed = position == 1
        elif 'Black' not in self._wires:
            self._disarmed = position == 2
        else:
            self._disarmed = position == 1

    def _cut_six(self, position):
        """
        If there are no yellow wires and the last digit of the serial number is odd, cut the third wire.
        Otherwise, if there is exactly one yellow wire and there is more than one white wire, cut the fourth wire.
        Otherwise, if there are no red wires, cut the last wire.
        Otherwise, cut the fourth wire.
        """
        if 'Yellow' not in self._wires and self._bomb.serial_number.is_odd:
            self._disarmed = position == 3
        elif self._wires.count('Yellow') == 1 and self._wires.count('White') > 1:
            self._disarmed = position == 4
        elif 'Red' not in self._wires:
            self._disarmed = position == 6
        else:
            self._disarmed = position == 4

    def _last_pos_of(self, color):
        return len(self._wires) - self._wires[::-1].index(color)


class ComplicatedWire(DisarmableModule):
    def __init__(self, position, disarmed, wires, bomb):
        super().__init__(position, disarmed)

        if not len(wires) == 6:
            raise ValueError('There must be exact six wires')

        self._wires = [(*w, False) for w in wires]
        self._bomb = bomb

    @property
    def wires(self):
        return self._wires

    def cut(self, position):
        if not self._wires[position][0]:
            return

        if not self._should_cut(*self._wires[position][:-1]):
            self.on_wrong_try()
            return

        self._wires[position] = self._wires[position][:-1] + (True,)
        self._disarmed = self._is_disarmed()

    def _is_disarmed(self):
        if self._disarmed:
            return True

        return all(w[-1] == self._should_cut(*w[:-1]) for w in self._wires)

    def _should_cut(self, colors, star, led):
        if not colors:
            return False

        red = 'Red' in colors
        blue = 'Blue' in colors
        batteries = self._bomb.battery_count
        parallel_port = PortType.PARALLEL in self._bomb.ports

        if star and not blue and not led:
            return True
        if (led and not red and not blue and not star) \
                or (led and blue and star and red) \
                or (blue and star and not red and not led):
            return False
        if blue and not star and (red or not led):
            return self._bomb.serial_number.is_even
        if (blue and led and not red) or (blue and red and star and not led):
            return parallel_port
        if (red and not blue and led) or (led and star and not red and not blue):
            return batteries >= 2
        return True


class WireSequence(DisarmableModule):
    _occurence_map = {
        'Red': 'C,B,A,AC,B,AC,ABC,AB,B'.split(','),
        'Blue': 'B,AC,B,AC,B,BC,C,AC,A'.split(','),
        'Black': 'ABC,AC,B,AC,B,BC,AB,C,C'.split(','),
    }

    def __init__(self, position, disarmed, wires):
        super().__init__(position, disarmed)

        if not len(wires) == 9:
            raise ValueError('There must be exact nine wires')

        if not all(w[0] in ('Red', 'Blue', 'Black') for w in wires if w):
            raise ValueError('Only Red, Blue and Black colors are allowed')

        if not all(w[1] in ('A', 'B', 'C') for w in wires if w):
            raise ValueError('Wire must be connected to either A, B or C')

        self._wires = [(*w, False) if w else None for w in wires]
        self._module = 0
        self._changes = 0

    @property
    def wires(self):
        return self._wires[self._module*3:(self._module+1)*3]

    @property
    def changes(self):
        return min(self._changes, 4)

    def next(self):
        self._change_module(min, 2, 1)

    def previous(self):
        self._change_module(max, 0, -1)

    def _change_module(self, extremum, extreme, addend):
        self._module = extremum(self._module + addend, extreme)
        self._changes += 1
        if self._changes > 4:
            self.on_wrong_try()

    def cut(self, position):
        assert position in range(3)

        abs_pos = self._module*3 + position
        assert self._wires[abs_pos]

        if not self._should_cut(abs_pos):
            self.on_wrong_try()
            return

        self._wires[abs_pos] = self._wires[abs_pos][:-1] + (True,)
        self._disarmed = self._is_disarmed()

    def _should_cut(self, abs_pos):
        color, connected, cut_ = self._wires[abs_pos]
        occurrence = sum([1 if w[0] == color else 0 for w in self._wires[:abs_pos] if w])
        return connected in WireSequence._occurence_map[color][occurrence]

    def _is_disarmed(self):
        if self._disarmed:
            return True
        return all(self._should_cut(i) == self._wires[i][2] for i in range(0, 9) if self._wires[i])
