from datetime import timedelta

from pyglet import clock

from bomb_defusal.modules.module import NeedyModule


class CapacitorDischarge(NeedyModule):
    def __init__(self, position, expiration):
        super().__init__(position, expiration)

        self._hold = False
        clock.schedule_interval(self._charge, 0.2)

    def _charge(self):
        if not self._hold:
            return

        if self._expiration >= self._duration:
            return

        self._expiration += timedelta(seconds=1)

    def hold(self):
        self._hold = True

    def release(self):
        self._hold = False
