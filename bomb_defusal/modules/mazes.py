from enum import Enum

from bomb_defusal.modules.module import DisarmableModule

R = 0b0001
T = 0b0010
L = 0b0100
B = 0b1000
RT, RL, RB = R | T, R | L, R | B
TL, TB, = T | L, T | B
LB = L | B
RTL, RTB, RLB = R | T | L, R | T | B, R | L | B
TLB = T | L | B

ShapeOne = [
    RB, RL, LB, RB, RL, L,
    TB, RB, TL, RT, RL, LB,
    TB, RT, LB, RB, RL, TLB,
    TB, R, RTL, TL, R, TLB,
    RTB, RL, LB, RB, L, TB,
    T, L, RT, TL, R, TL
]
ShapeTwo = [
    R, RLB, L, LB, RLB, L,
    LB, TL, RB, TL, RT, LB,
    TB, RB, TL, RB, RL, TLB,
    RTB, TL, RB, TL, B, TB,
    TB, B, TB, RB, TL, TB,
    T, RT, TL, RT, RL, TL
]
ShapeThree = [
    RB, RL, LB, B, RB, LB,
    T, B, TB, RT, TL, TB,
    RB, TLB, TB, RB, LB, TB,
    TB, TB, TB, TB, TB, TB,
    TB, RT, TL, TB, TB, TB,
    RT, RL, RL, TL, RT, TL
]
ShapeFour = [
    RB, LB, R, RL, RL, LB,
    TB, TB, RB, RL, RL, TLB,
    TB, RT, TL, RB, L, TB,
    TB, R, RL, RTL, RL, TLB,
    RTB, RL, RL, RL, LB, TB,
    RT, RL, L, R, TL, T
]
ShapeFive = [
    R, RL, RL, RL, RLB, LB,
    LB, RL, RL, RLB, TL, T,
    RTB, LB, R, TL, RB, LB,
    TB, RT, RL, LB, T, TB,
    TB, RB, RL, RTB, L, TB,
    T, RT, RL, RL, RL, TL
]
ShapeSix = [
    B, RB, LB, R, RLB, LB,
    TB, TB, TB, RB, TL, TB,
    RTB, TL, T, TB, RB, TL,
    RT, LB, RB, TLB, TB, B,
    RB, TL, T, TB, RT, TLB,
    RT, RL, RL, TL, R, TL
]
ShapeSeven = [
    RB, RL, RL, LB, RB, LB,
    TB, RB, L, RT, TL, TB,
    RT, TL, RB, L, RB, TL,
    RB, LB, RTB, RL, TL, B,
    TB, T, RT, RL, LB, TB,
    RT, RL, RL, RL, RTL, TL
]
ShapeEight = [
    B, RB, RL, LB, RB, LB,
    RTB, RTL, L, RT, TL, TB,
    TB, RB, RL, RL, LB, TB,
    TB, RT, LB, R, RTL, TL,
    TB, B, RT, RL, RL, L,
    T, RTL, RL, RL, RL, L
]
ShapeNine = [
    B, RB, RL, RL, RLB, LB,
    TB, TB, RB, L, TB, TB,
    RTB, RTL, TL, RB, TL, TB,
    TB, B, RB, TL, R, TLB,
    TB, TB, TB, RB, LB, T,
    RT, TL, RT, TL, RT, L
]


class MazesShape(Enum):
    One = (ShapeOne, (0, 1), (5, 2))
    Two = (ShapeTwo, (1, 3), (4, 1))
    Three = (ShapeThree, (3, 3), (5, 3))
    Four = (ShapeFour, (0, 0), (0, 3))
    Five = (ShapeFive, (4, 2), (3, 5))
    Six = (ShapeSix, (2, 4), (4, 0))
    Seven = (ShapeSeven, (1, 0), (1, 5))
    Eight = (ShapeEight, (3, 0), (2, 3))
    Nine = (ShapeNine, (2, 1), (0, 4))


class Mazes(DisarmableModule):
    def __init__(self, position, disarmed, shape, start_pos, end_pos):
        super().__init__(position, disarmed)

        if start_pos[0] not in range(0, 6):
            raise ValueError('Starting x-position must be in between 0 and 5.')

        if start_pos[1] not in range(0, 6):
            raise ValueError('Starting y-position must be in between 0 and 5.')

        if end_pos[0] not in range(0, 6):
            raise ValueError('End x-position must be in between 0 and 5.')

        if end_pos[1] not in range(0, 6):
            raise ValueError('End y-position must be in between 0 and 5.')

        if not isinstance(shape, MazesShape):
            raise ValueError('Invalid mazes shape provided')

        self._shape = shape.value[0]
        self._marker = shape.value[1:]
        self._start_pos = start_pos
        self._end_pos = end_pos
        self._pos = self._start_pos

    @property
    def marker(self):
        return self._marker

    @property
    def cursor(self):
        return self._pos

    @property
    def abs_cursor(self):
        return self.cursor[0] + self.cursor[1] * 6

    @property
    def target(self):
        return self._end_pos

    def move(self, direction):
        if self.disarmed:
            return

        directions = dict(zip('RULD', [1, 2, 4, 8]))
        assert direction in directions.keys()
        dir_ = directions[direction]

        piece = self._shape[self.abs_cursor]
        if not piece & dir_:
            self.on_wrong_try()
            return

        if direction == 'U':
            self._pos = self._pos[0], self._pos[1] - 1
        if direction == 'D':
            self._pos = self._pos[0], self._pos[1] + 1
        if direction == 'R':
            self._pos = self._pos[0] + 1, self._pos[1]
        if direction == 'L':
            self._pos = self._pos[0] - 1, self._pos[1]

        if self._pos == self._end_pos:
            self._disarmed = True
