from bomb_defusal.view.module import Module


class Battery(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`Battery` class.
        Depending on the number of batteries to show, a different 3D model is loaded

        :param bomb_defusal.modules.Battery model: Model of the battery module
        """
        super().__init__(model, base='resources/components/battery{}.obj'.format(model.units))
