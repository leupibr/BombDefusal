from bomb_defusal.view.components import Text
from bomb_defusal.view.module import Module


class SerialNumber(Module):
    def __init__(self, model):
        """
        Initializes a new instance of the :class:`SerialNumber` class.

        :param bomb_defusal.modules.SerialNumber model: Model of the serial number module
        """
        super().__init__(model, base='resources/serial.obj')
        self._number = Text((0, 0, 0), text_height=.2, text_font='resources/fonts/a-team.ttf', offset=0)
        self._number.text = str(model).lower()

    def on_draw(self):
        """Draw the serial number 3D view"""
        self._number.draw()
